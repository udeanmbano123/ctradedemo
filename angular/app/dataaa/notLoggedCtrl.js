app.controller('notLoggedCtrl', function ($scope, $interval, $rootScope, ngDialog, $location, $http, Data ,$timeout) {
    //initially set those objects to null to avoid undefined error
    $scope.home = {};
    $scope.initbalance = "0.00";

    $scope.openDiag = function () {
        ngDialog.open({
            template: 'firstDialog',
            className: 'ngdialog-theme-default ngdialog-theme-custom'         
        });
    };

    $scope.checkPrice = function (price) {
        alert(price ) ;

    };

    $scope.openDiagForget = function () {
        ngDialog.open({
            template: 'forgetPassDialog',
            className: 'ngdialog-theme-default ngdialog-theme-custom'         
        });
    };


    Data.get('MarketWatchbidoffer').then(function (results) {
           $scope.marketwatch = results ; 
           // $scope.predicateme = 'id';  
           // $scope.reverseme = true;  
           // $scope.currentPageme = 1;  
           // $scope.orderme = function (predicateme) {  
           //   $scope.reverseme = ($scope.predicateme === predicateme) ? !$scope.reverseme : false;  
           //   $scope.predicateme = predicateme;  
           // };  
           // $scope.totalItemsme = $scope.marketwatch.length;  
           // $scope.numPerPageme = 5;              
    });
    // $scope.paginateme = function (valueme) {  
    //  var beginme, endme, indexme;  
    //  beginme = ($scope.currentPageme - 1) * $scope.numPerPageme;  
    //  endme = beginme + $scope.numPerPageme;  
    //  indexme = $scope.marketwatch.indexOf(valueme);  
    //  return (beginme <= indexme && indexme < endme);  
    // };   

    Data.get('MarketWatchZSE').then(function (results) {
            $scope.marketwatchzse = results ; 
           $scope.predicatemez = 'id';  
           $scope.reversemez = true;  
           $scope.currentPagemez = 1;  
           $scope.ordermez = function (predicatemez) {  
             $scope.reversemez = ($scope.predicatemez === predicatemez) ? !$scope.reversemez : false;  
             $scope.predicatemez = predicatemez;  
           };  
           $scope.totalItemsmez = $scope.marketwatchzse.length;  
           $scope.numPerPagemez = 10;      

    });
    
    $scope.paginatemez = function (valuemez) {  
     var beginmez, endmez, indexmez;  
     beginmez = ($scope.currentPagemez - 1) * $scope.numPerPagemez;  
     endmez = beginmez + $scope.numPerPagemez;  
     indexmez = $scope.marketwatchzse.indexOf(valuemez);  
     return (beginmez <= indexmez && indexmez < endmez);  
    };   

    var updateClockzz = function() {
       Data.get('MarketWatchbidoffer').then(function (results) {
                $scope.marketwatch = results ; 
        });
       Data.get('MarketWatchZSE').then(function (results) {
                $scope.marketwatchzse = results ; 
        });
    };
    $scope.graph_company = "OMZIL" ;
    $scope.graph_company_ZSE = "CBZ.ZW" ;

    $interval(updateClockzz, 60000);


       $scope.boxes = [
            {title: 'ZSE'},
            {title: 'Market Cap: US$8,288,175,961'},
            {title: 'Turnover:   US$4,260,069.11'},
            {title: 'Trades: 63'},
            {title: ' --- FINSEC'},
            {title: 'Market Cap: US$8,288,175,961'},
            {title: 'Turnover:   US$4,260,069.11'},
            {title: 'Trades: 63'}
        ];
        $scope.moving = false;

        $scope.moveLeft = function() {
            $scope.moving = true;
            $timeout($scope.switchFirst, 1000);
        };
        $scope.switchFirst = function() {
            $scope.boxes.push($scope.boxes.shift());
            $scope.moving = false;
            $scope.$apply();
        };

        $interval($scope.moveLeft, 2000);


    $scope.tableRowClicked=function(data){
        console.log(data)
        if($scope.graph_company_ZSE != data){
            $scope.graph_company_ZSE = "Loading data ....." ; 
            Data.getn('online.ctrade_php/getPrices.php?company='+data).then(function (results) {
                 //alert("Data Loaded for " + data) ;  
                $scope.graph_company_ZSE = data ; 
                console.log(results) ;
                $scope.data_zse = results ; 
                if($scope.rc.api == undefined){
                    console.log('Error :', $scope.rc.api);
                }else{
                    $scope.rc.api.refresh();
                }

            });
        }else{
            alert("Already Loaded graph") ;
        }

    }

    $scope.options = {
        chart: {
            key:'Prices',
            type: 'lineChart',
            height: 250,
            margin : {
                top: 10,
                right: 5,
                bottom: 50,
                left: 40
            },
            x: function(d){return d[0];},
            y: function(d){return d[1];},              
            useVoronoi: false,
            clipEdge: true,
            duration: 100,
            useInteractiveGuideline: true,
            xAxis: {
                showMaxMin: false,                    
                tickFormat: function(d) {
                    return d3.time.format('%x')(new Date(d))
                }
            },
            yAxis: {
                showMaxMin: true,                                        
                tickFormat: function(d){
                    return d3.format(',.4f')(d);
                }
            },
            zoom: {
                enabled: true,
                scaleExtent: [1, 10],
                useFixedDomain: false,
                useNiceScale: false,
                horizontalOff: false,
                verticalOff: true,
                unzoomEventType: 'dblclick.zoom'
            }
        }
    };

    $scope.options1 = {
        chart: {
            type: 'lineChart',
            height: 150,
            margin : {
                top: 10,
                right: 5,
                bottom: 50,
                left: 40
            },
            x: function(d){return d[0];},
            y: function(d){return d[1];},              
            useVoronoi: false,
            clipEdge: true,
            duration: 100,
            useInteractiveGuideline: true,
            xAxis: {
                showMaxMin: false,                    
                tickFormat: function(d) {
                    return d3.time.format(' %x')(new Date(d))
                }
            },
            yAxis: {
                showMaxMin: true,                                        
                tickFormat: function(d){
                    return d3.format(',.4f')(d);
                }
            },
            zoom: {
                enabled: true,
                scaleExtent: [1, 10],
                useFixedDomain: false,
                useNiceScale: false,
                horizontalOff: false,
                verticalOff: true,
                unzoomEventType: 'dblclick.zoom'
            }

        }
    };

    $scope.rc = {} ;

    $scope.getDataServer = function(companyName) { 
        
        $scope.graph_company = companyName ; 
        Data.getn('online.ctrade_php/getPrices.php?company='+companyName).then(function (results) {
             alert("Data Loaded for " + companyName) ; 
            console.log(results) ;
            $scope.data = results ; 
            console.log('scope api:', $scope.rc.api);
            if($scope.rc.api == undefined){
                console.log('Error :', $scope.rc.api);
            }else{
                $scope.rc.api.refresh();
            }

        });


        // $scope.rc.api.getScope().chart.xAxis.tickValues(function () { return arr });
        // $scope.api.refresh(); 
        //$scope.$apply();
    };

    // setTimeout(function(){
    //     console.log('scope api:', $scope.rc.api);
    //     $scope.rc.api.refresh();
    // });
    //http://localhost/CTRADEAPI/subscriber/getMyPortFolio?cdsNumber=304879687195838
    Data.get('GetCompaniesListForGraphAll').then(function (results) {
        $scope.companies_listing =  results ;
       $scope.predicatec = 'Names';  
       $scope.reversec = true;  
       $scope.currentPagec = 1;  
       $scope.orderc = function (predicatec) {  
         $scope.reversec = ($scope.predicatec === predicatec) ? !$scope.reversec : false;  
         $scope.predicatec= predicatec;  
       };  
       $scope.totalItemsc = $scope.companies_listing.length;  
       $scope.numPerPagec = 10;  
    });

       $scope.paginatec = function (valuec) {  
         var beginc, endc, indexc;  
         beginc = ($scope.currentPagec - 1) * $scope.numPerPagec;  
         endc = beginc + $scope.numPerPagec;  
         indexc = $scope.companies_listing.indexOf(valuec);  
         return (beginc <= indexc && indexc < endc);  
       };     
        // d3.json('http://localhost:84/CTRADEAPI/Subscriber/GetCompaniesPriceList?company=OMZIL', function(error, data) {
        // if (error) return console.error(error);
        // console.log(data);
        //  });
        // $scope.data = [

        //     {
        //         "key" : "Current" ,
        //         "area": true,
        //         "values" :  [[1480892400,0.8207],[1480978800,0.8207],[1481151600,0.8207],[1481583600,0.8207],[1481670000,0.8207],[1481756400,0.8207],[1481842800,0.8207],[1482188400,0.8207],[1482966000,0.8207],[1483484400,0.8207],[1483916400,0.8207],[1484175600,0.8207],[1484262000,0.8207],[1485730800,0.8207],[1486335600,0.8207],[1486422000,0.8207],[1486681200,0.8207],[1487631600,0.8207],[1487890800,0.8207],[1489359600,0.8207],[1489705200,0.8207],[1490310000,0.825],[1490824800,0.825],[1490911200,0.825],[1491256800,0.825],[1491429600,0.825],[1491948000,0.825],[1492984800,0.825],[1493244000,0.825],[1493676000,0.825],[1493935200,0.825],[1494540000,0.825],[1495576800,0.825],[1496354400,0.825],[1496613600,0.825],[1496786400,0.825],[1497391200,0.825],[1497564000,0.825],[1497909600,0.825],[1498600800,0.825],[1499032800,0.825],[1499983200,0.825],[1501452000,0.825],[1501538400,0.825],[1501797600,0.825],[1502143200,0.825],[1502920800,0.825],[1503266400,0.825],[1503352800,0.825],[1504648800,0.825],[1505426400,0.825] ]
        //     }
        // ] 

        // ;

        Data.getn('online.ctrade_php/getPrices.php?company=OMZIL').then(function (results) {
                // console.log(results) ;
                $scope.data = results ; 
        });
        Data.getn('online.ctrade_php/getPrices.php?company=CBZ.ZW').then(function (results) {
                // console.log(results) ;
                $scope.data_zse = results ; 
        });

        $scope.run = true;
});

