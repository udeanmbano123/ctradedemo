app.controller('OrderCtrl', function($scope, $rootScope, $sessionStorage, ngDialog, $location, $http, Data) {
    $scope.list = [];
    $scope.my_broker_name ;
    $scope.broker_set ;
    $scope.brokerz ; 
    $scope.marko ; 
    $scope.oq ; 
    $scope.isDisabled_sell = false;
    $scope.isDisabled_buy = false;

    $scope.searchButtonText_buy  = "Place Order ► "; 
    $scope.searchButtonText_sell  = "Place Order ► "; 
    if( $sessionStorage.broker != '') {

    }else{
        //http://192.168.3.69/CTRADEAPI/subscriber/GetBrokers
        Data.get('GetBrokers').then(function (results) {
            $scope.brokerz =  results ;
        });
    }


    $scope.macompanies_listing; 
    //http://localhost/CTRADEAPI/subscriber/getMyPortFolio?cdsNumber=304879687195838
    Data.get('GetCompaniesListForGraphAll').then(function (results) {
        $scope.macompanies_listing =  results ;
    });
    $scope.updatePrice = function () {
        // alert("Hello") ;
        // $scope.myprice = 0 ;
        $scope.price = parseFloat($scope.company.split(',')[1]);
        $scope.total =  $scope.quantity *  $scope.price ; 
        $scope.charges_buy = $scope.total * 0.01693 ; 
        $scope.charges_sell = $scope.total * 0.02443 ;         
    };
    
    $scope.updateMarketOrder = function () {
        alert("Hello" +$scope.marko ) ;
    };

    $scope.test = function () {
        $scope.price = parseFloat($scope.company.split(',')[1]);
        $scope.total =  $scope.quantity *  $scope.price ; 
        $scope.charges_buy = $scope.total * 0.01693 ; 
        $scope.charges_sell = $scope.total *  0.02443 ;    
    };

    $scope.company;
    $scope.security;
    $scope.order_type;
    $scope.tif = "Good Till Cancelled (GTC)";
    $scope.orderType = "Day";
    $scope.quantity = 0;
    $scope.total_ammount  ;
    // $scope.price = $scope.company.split(',')[1];
    $scope.price ;
    $scope.cdsNumber;
    
    $scope.source = "online" ;
    $scope.cds = $sessionStorage.cds ;
    $scope.brokerThere = $sessionStorage.broker ;
    $scope.tinashe_my_style = "" ; 
   $scope.cancelOrder = function (id , o_type) {
        Data.get('UpdateCancelOrder?cdsnumber='+ $sessionStorage.cds +'&ordernumber='+id +'&orderType='+o_type).then(function (results) {
            alert(results) ; 
        });
        
    };
    
    if($scope.brokerThere == ''){
        //alert("No broker" + $scope.brokerxs) ;         
    }else{
        $scope.broker_set = $sessionStorage.broker ; 
        $scope.tinashe_my_style = "display:none;" ;
    }

    
    //http://localhost/CTRADEAPI/subscriber/GetBrokers
    $scope.doOrder = function (orderTyle) {

        $scope.isDisabled_buy = true;
        $scope.isDisabled_sell = true;
        $scope.searchButtonText_buy = "Please Wait"; 
        $scope.searchButtonText_sell = "Please Wait"; 

        if($scope.company == null ){
                alert("Please select company " );
        }     
        if($scope.brokerThere == ''){
            if($scope.my_broker_name != null) {
                $scope.broker_set = $scope.my_broker_name.Code ; 
            }else{
                
                // alert("Please select broker first") ;
            }
        }

        var upperPrice = $scope.company.split(',')[1] * 1.1;
        var lowerPrice = $scope.company.split(',')[1] * 0.9;

        if ($scope.price >= upperPrice || $scope.price <= lowerPrice) {
                alert("Please enter a price that is within the Price Protection range of +/-10% of the current price " );
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";    
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;                            
        }else if($scope.price == 0 || $scope.price == null || $scope.price == undefined || $scope.price == "" ||    $scope.price.length == 0){
                alert("Please enter correct price " );
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";    
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;                              
        }else if($scope.quantity == 0 || $scope.quantity == null || $scope.quantity == undefined || $scope.quantity == "" ||    $scope.quantity.length == 0){
                alert("Please enter correct quantity " );
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";    
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;                                  
        } 
        //alert( (( * 0.1) + $scope.company.split(',')[1] )  + " - " +  $scope.price) ;
        //http://localhost/CTRADEAPI/subscriber/OrderPosting?company=TESTCOMP1&security=EQUITY&orderTrans=BUY&orderType=Day&quantity=9800&price=1.1&cdsNumber=000000000103691&broker=WESTS
        else{
                Data.get('OrderPosting?company='+$scope.company.split(',')[0]+'&security='+$scope.security+'&orderTrans='+orderTyle+'&orderType=Day&quantity='+$scope.quantity+'&price='+$scope.price+'&cdsNumber='+$scope.cds+'&broker='+$scope.broker_set+'&source='+$scope.source ).then(function (results) {
                // alert(results) ;
                $scope.company = ",0.0";
                $scope.security = "";
                $scope.order_type = "";
                $scope.quantity = "";
                $scope.price = "";
                $scope.cdsNumber = "";
                $scope.broker_set = "";
                ngDialog.closeAll();
                if(results == "1"){
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";    
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;                
                    alert("Order successfully placed");
                }
                else {
                    $scope.isDisabled_buy = false;
                    $scope.isDisabled_sell = false;    
                    $scope.searchButtonText_buy  = "Place Order ► "; 
                    $scope.searchButtonText_sell  = "Place Order ► ";                     
                    alert(results);
                }
            });
        }

    };

});




// app.controller('FirstDialogCtrl', function ($scope, ngDialog) {
//                 $scope.next = function () {
//                     ngDialog.close('ngdialog1');
//                     ngDialog.open({
//                         template: 'secondDialog',
//                         className: 'ngdialog-theme-flat ngdialog-theme-custom'
//                     });
//                 };
//             });
//http://localhost/CTRADEAPI/subscriber/?any=833494026089143
//http://localhost/CTRADEAPI/subscriber/Login?username=fariraishe@escrowgroup.org&password=f
