app.directive('focus', function() {
    return function(scope, element) {
        element[0].focus();
    }      
});

angular.module('blink', [])
.directive('blink', ['$interval', function($interval) {
  return function(scope, element, attrs) {
      var timeoutId;
      
      var blink = function() {
        element.css('visibility') === 'hidden' ? element.css('visibility', 'inherit') : element.css('visibility', 'hidden');
      }
      
      timeoutId = $interval(function() {
        blink();
      }, 1000);
    
      element.css({
        'display': 'inline-block'
      });
      
      element.on('$destroy', function() {
        $interval.cancel(timeoutId);
      });
    };
}]);

app.directive('passwordMatch', [function () {
    return {
        restrict: 'A',
        scope:true,
        require: 'ngModel',
        link: function (scope, elem , attrs,control) {
            var checker = function () {
 
                //get the value of the first password
                var e1 = scope.$eval(attrs.ngModel); 
 
                //get the value of the other password  
                var e2 = scope.$eval(attrs.passwordMatch);
                if(e2!=null)
                return e1 == e2;
            };
            scope.$watch(checker, function (n) {
 
                //set the form control to valid if both 
                //passwords are the same, else invalid
                control.$setValidity("passwordNoMatch", n);
            });
        }
    };
}]);





app.directive('extendedLineChart', function(){
    "use strict";
   return {
       restrict: 'E',
       require: '^nvd3SparklineChart',
       link: function($scope, $element, $attributes, nvd3SparklineChart) {
           $scope.d3Call = function(data, chart){

               var svg = d3.select('#' + $scope.id + ' svg')
                   .datum(data);

               var path = svg.selectAll('path');
                    path.data(data)
                    .transition()
                    .ease("linear")
                    .duration(500)

               return svg.transition()
                   .duration(500)
                   .call(chart);

           }
       }
   }


});