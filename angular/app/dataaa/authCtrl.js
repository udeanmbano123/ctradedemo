app.controller('authCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data) {
    //initially set those objects to null to avoid undefined error
    $scope.login = {};
    $scope.signup = {};
    $scope.doLogin = function (customer) {
        Data.post('login', {
            customer: customer
        }).then(function (results) {
            Data.toast(results);
            alert(results) ;
            if (results.status == "success") {
                $location.path('dashboard');
            }
        });
    };
    $scope.signup = {email:'',password:'',name:'',phone:'',address:''};
    $scope.signUp = function (customer) {
        Data.post('signUp', {
            customer: customer
        }).then(function (results) {
            Data.toast(results);
            if (results.status == "success") {
                $location.path('dashboard');
            }
        });
    };
    
    $scope.persons = [{id: 1, name: 'Alex'}, {id: 2, name: 'David'}];

    $scope.logout = function () {
        $sessionStorage.id ="" ;
        $sessionStorage.username = "" ;
        $sessionStorage.email =""  ;            
        $sessionStorage.cds =""  ;  
    }
});