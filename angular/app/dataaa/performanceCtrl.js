app.controller('performanceCtrl', function($scope, $rootScope, $sessionStorage, ngDialog, $location, $http, Data) {
    $scope.list = [];

    $scope.brokerz ; 
    //http://192.168.3.69/CTRADEAPI/subscriber/GetBrokers
    Data.get('GetBrokers').then(function (results) {
        $scope.brokerz =  results ;
    });
    $scope.macompanies_listing; 
    //http://localhost/CTRADEAPI/subscriber/getMyPortFolio?cdsNumber=304879687195838
    Data.get('GetCompaniesListForGraphAll').then(function (results) {
        $scope.macompanies_listing =  results ;
    });
    $scope.company;
    $scope.security;
    $scope.order_type;
    $scope.orderType = "Day";
    $scope.quantity;
    $scope.price;
    $scope.cdsNumber;
    $scope.broker = "AKRI";
    $scope.source = "online" ;
    $scope.cds = $sessionStorage.cds ;
    //http://localhost/CTRADEAPI/subscriber/GetBrokers
    $scope.doOrder = function (orderTyle) {
        var upperPrice = $scope.company.split(',')[1] * 1.1;
        var lowerPrice = $scope.company.split(',')[1] * 0.9;

        //                          currentPrice
        if ($scope.price >= upperPrice || $scope.price <= lowerPrice) {
                alert("Please enter a price that is within the Price Protection range of +/-10% of the current price" );
        }        

        //alert( (( * 0.1) + $scope.company.split(',')[1] )  + " - " +  $scope.price) ;
        //http://localhost/CTRADEAPI/subscriber/OrderPosting?company=TESTCOMP1&security=EQUITY&orderTrans=BUY&orderType=Day&quantity=9800&price=1.1&cdsNumber=000000000103691&broker=WESTS
        else{
                Data.get('OrderPosting?company='+$scope.company.split(',')[0]+'&security='+$scope.security+'&orderTrans='+orderTyle+'&orderType=Day&quantity='+$scope.quantity+'&price='+$scope.price+'&cdsNumber='+$scope.cds+'&broker=AKRI&source='+$scope.source ).then(function (results) {
                // alert(results) ;
                $scope.company = "";
                $scope.security = "";
                $scope.order_type = "";
                $scope.quantity = "";
                $scope.price = "";
                $scope.cdsNumber = "";
                $scope.broker = "";
                ngDialog.closeAll();
                if(results == "1"){
                    
                    alert("Order successfully placed");
                }
                else {
                    alert(results);
                }
            });
        }

    };

});




// app.controller('FirstDialogCtrl', function ($scope, ngDialog) {
//                 $scope.next = function () {
//                     ngDialog.close('ngdialog1');
//                     ngDialog.open({
//                         template: 'secondDialog',
//                         className: 'ngdialog-theme-flat ngdialog-theme-custom'
//                     });
//                 };
//             });
//http://localhost/CTRADEAPI/subscriber/?any=833494026089143
//http://localhost/CTRADEAPI/subscriber/Login?username=fariraishe@escrowgroup.org&password=f
