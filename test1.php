<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>File upload tester</title>
</head>
<script>

var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];    
function ValidateSingleInput(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
		if(oInput.files[0].size > 5 * 1024 * 1024){
			 alert("Max file size is 5MB.");
   oInput.value = "";
                return false;
		}
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                oInput.value = "";
                return false;
            }
        }
    }
	ValidateSize(oInput);
    return true;
}
var _validFileExtensions2 = [".jpg", ".jpeg", ".bmp", ".gif", ".png",".doc",".docx",".pdf"]; 
function ValidateSingleInput2(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
		
		if(oInput.files[0].size > 5 * 1024 * 1024){
			 alert("Max file size is 5MB.");
   oInput.value = "";
                return false;
		}
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions2.length; j++) {
                var sCurExtension = _validFileExtensions2[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions2.join(", "));
                oInput.value = "";
                return false;
            }
        }
    }
		ValidateSize(oInput);
    return true;
}
</script>
<body>
<form action="tests.php" method="post" enctype="multipart/form-data">
     Upload Profile Photo:<i>Max file size 5mb</i>
    <input type="file" name="Profile" id="Profile" onchange="ValidateSingleInput(this);">
	 Upload National ID:<i>Max file size 5mb</i>
	<input type="file" name="NationalId" id="NationalId"  onchange="ValidateSingleInput2(this);">
	 Upload Proof of Residence:<i>Max file size 5mb</i>
	<input type="file" name="ProofOfResidence" id="ProofOfResidence"  onchange="ValidateSingleInput2(this);">
    <input type="submit" name="submit" value="Upload">
</form>
</body>

</html>