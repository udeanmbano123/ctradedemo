<?php
//$email_add = filter_input(INPUT_GET, 'email', FILTER_SANITIZE_URL) ;
$email_add =  htmlspecialchars(@$_GET['email'], ENT_QUOTES, 'UTF-8');
$clubid =  htmlspecialchars(@$_GET['club_id'], ENT_QUOTES, 'UTF-8');
$clubname =  htmlspecialchars(@$_GET['clubname'], ENT_QUOTES, 'UTF-8');
function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

$encrypted_txt = encrypt_decrypt('decrypt', $email_add);
include("online.ctrade_php/db_cds/DbConnectCDS.php"); 
$db_cdsc = new DbConnectCDS();
    $conn_cdsc = $db_cdsc->connect();
	$decisiondone=searchExsist( $conn_cdsc ,$email_add);
	
	if($decisiondone==false){
		header("Location: https://demo.ctrade.co.zw/ctrade_v2/online.ctrade_php/sign_up.php") ;
	}
function searchExsist($conn ,$my_email) {
	$numrows=false;
	$sql1 = "select Email from [CDS_ROUTER].[dbo].[Accounts_Clients_Web] where Email=?" ; 
	$result = sqlsrv_query($conn,$sql1,Array($my_email));
    $rows = sqlsrv_has_rows( $result );
	//echo "<br>row count =".$rows."<br>".$sql1 ; 
	if($rows>0){
		$numrows=true;
	}else{
	$numrows=false;
	}
	return $numrows; 
}	
?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<html lang="en" ng-app="myApp">
    <head>
       <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@CTRADEmobile">
    <meta name="twitter:creator" content="@CTRADEmobile">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="C-TRADE">
    <meta name="twitter:description" content="The first online and mobile platform for the trading of shares on Zimbabwean stock markets...">
    <meta name="twitter:image" content="https://ctrade.co.zw/wp-content/uploads/2018/02/c-trade-logo.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://ctrade.co.zw/online">
    <meta property="og:title" content="DashForge">
    <meta property="og:description" content="The first online and mobile platform for the trading of shares on Zimbabwean stock markets...">
    <meta property="og:image" content="https://ctrade.co.zw/wp-content/uploads/2018/02/c-trade-logo.png">
    <meta property="og:image:secure_url" content="https://ctrade.co.zw/wp-content/uploads/2018/02/c-trade-logo.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="C-trade online is a designed to enable retail investors to carry out direct trading and portfolio management. C-Trade automates the interaction of an individual with the securities dealers. It offers a wide range of online and mobile payment options for the funding buy orders.">

    <meta name="author" content="C-Trade">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <title>C-Trade Online Investment Club Invitation Confirmation form</title>

        <!-- Vendor CSS -->
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">     
        <link rel="icon" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon-150x150.png" sizes="32x32" />
        <link rel="icon" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon.png" />
    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.0/chosen.jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.0/chosen.proto.min.js"></script>
<script src="amcharts/amcharts/amcharts.js"></script>
<script src="amcharts/amcharts/pie.js"></script>
<script src="amcharts/amcharts/serial.js"></script>
<script src="amcharts/amcharts/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<script src="amcharts/amcharts/themes/light.js"></script>
<script src="amcharts/amcharts/plugins/dataloader/dataloader.js" type="text/javascript"></script>
   <script>
history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
	</script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122905596-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-122905596-1');
        </script>
        <style>
            /* Needed for hiding crollbars when pushing */


            .ng-pageslide {
                background: #000;
            }

            body.ng-pageslide-body-open::before {
                content: '.';
                display: block;
                position: absolute;
                top: 0;
                background-color: rgb(0,0,0);
                left: 0;
                right: 0;
                bottom: 0;
                z-index: 1;
                opacity: 0.5;
                transition: opacity 1s;
                opacity: 0.5;
                pointer-events: all;
            }

            body.ng-pageslide-body-closed::before {
                transition: opacity 1s;
                content: '.';
                display: block;
                position: absolute;
                top: 0;
                background-color: rgb(0,0,0);
                left: 0;
                right: 0;
                bottom: 0;
                z-index: 1;
                opacity: 0;
                pointer-events: none;
            }

            #container1, #container2 {
                width: 100%;
                height: 20rem;
                border: 1px solid #000;

                p {
                    font-size: 0.5rem!important;
                }
            }

        </style>

        <!-- CSS -->
        <link href="css/app_1.min.css" rel="stylesheet">   
        <link href="css/chosen.min.css" rel="stylesheet">   
        <link href="css/ngProgress.css" rel="stylesheet">   
        <link href="css/toaster.min.css" rel="stylesheet">   
        <link href="css/angular-confirm.min.css" rel="stylesheet">   
        <link rel="image_src" href="css/chosen-sprite.png"> 
        <link rel="stylesheet" href="vendors/ngDialog/css/ngDialog.min.css">
        <link rel="stylesheet" href="vendors/ngDialog/css/ngDialog-theme-default.min.css">
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5aba26f8d7591465c708f353/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->        
    </head>
    
    <body>
            <header id="header" class="clearfix">
                <ul class="h-inner">
                    <li class="hidden-xs" ng-controller="tempdataCtrl">
                        <a ng-if="cds == ''" href="#/home" class="p-l-10">
                            <img src="img/demo/logo4.png" style="width: 34%;" alt="">
                        </a>
                        <a ng-if="cds !== ''" href="#" onclick="event.preventDefault();" class="p-l-10">
                            <img src="img/demo/logo4.png" style="width: 34%;" alt="">
                        </a>
                    </li>

                    <li class="pull-right">
                        <ul class="hi-menu">


                            <li class="dropdown" ng-controller="tempdataCtrl" >
                                <toaster-container toaster-options="{'close-button':false, 'time-out':{ 'toast-warning': 200, 'toast-error': 200 , 'toast-success': 200 } }"></toaster-container>
                                <a data-toggle="dropdown" href="">

                                    <span class="him-label"><img style="width: 60%;float: right;z-index: 9999;padding: 0px;margin-top: -10px;" src = "img/ctrader1.png">
									<br/><b style="font-size:8pt;width: 60%;float: right;z-index: 9999;padding: 0px;margin-top: -10px;text-transform: uppercase"  ng-bind="myname" id="myname"   class ="cd-signin" href="#"  ></b>
									</span>

                                </a>


                                <ul class="dropdown-menu dm-icon pull-right">
                                    <li class="hidden-xs">
                                        <a data-ma-action="fullscreen" href="">Toggle Fullscreen</a>
                                    </li>

                                    <li ng-if="Login_Name == 'Guest' ">
                                        <a class ="cd-signin" href="#" onclick="event.preventDefault();" ng-controller="signindialogCtrl" ng-click="openDiag()"> Login</a>
                                    </li>
                                    <li ng-if="Login_Name == 'logged' ">
                                        <a class ="cd-signin" href="#" onclick="event.preventDefault();" ng-controller="tempdataCtrl" ng-click="logout()" > Logout </a>
                                    </li>
                                    <li ng-if="Login_Name == 'Guest' ">
                                        <a class ="cd-signup"  href="online.ctrade_php/sign_up.php">Create Account</a>
                                    </li>
                                    <li ng-if="cds !== '' ">
                                        <a href="">Settings</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                

                </ul>

            </header>

   
             <div class="container" >
					  <div class="row" style="margin-top:20%">
            <!-- Login -->
			
           <form  class="form-horizontal" style="font-size:11pt;width:400px;margin-left:30%" action ="online.ctrade_php/func/investment_pass.php" method="post">
           

      <div class="form-group">
                    <label class="control-label md-2"><h1>Please confirm or reject to join the Investment Club</h1>
                    
                    <?php 
                    echo $clubname ;
                    ?>
                    <input type="hidden" value="<?php echo $email_add ;?>" name ="keyz">
					<input type="hidden" value="<?php echo $clubid ;?>" name ="clubid"></label>
					</div>
                    <div class="form-group">
                   <input type="radio" id="group" name="group" value="C" required> Confirm
                   <input type="radio" id="group" name="group" value="R" r> Reject
                    </div>
    <div class="form-group">
                    <button class="btn btn-primary" style="background:#000000" type ="submit">Confirm</button>
</div>
            </form>
       
        </div>
					
                    </div>                        
               


        



        <!-- Javascript Libraries -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>



        <script src="js/functions.js"></script>
        
    <style>
			.marquee {
  width:100%;
  overflow: hidden;
  border: 1px solid green;
  background:none;
}
			</style>
<style>
.marquee {
  margin: 0 auto;
  width: 100%; // or 100% inside a container
  height: 30px;
  white-space: nowrap;
  overflow: hidden;
  box-sizing: border-box;
  position: relative;
  
  &:before, &:after {
    position: absolute;
    top: 0;
    width: 50px;
    height: 30px;
    content: "";
    z-index: 1;
  }
  &:before {
    left: 0;
    background: linear-gradient(to right, white 5%, transparent 100%);
  }
  &:after {
    right: 0;
    background: linear-gradient(to left, white 5%, transparent 100%);
  }
}

.marquee__content {
  width: 300% !important;
  display: flex;
  line-height: 30px;
  animation: marquee 35s linear infinite forwards;
  &:hover {
    animation-play-state: paused;
  } 
}

.list-inline {
  display: flex;
  justify-content: space-around;
  width: 33.33%;
  
  /* reset list */
  list-style: none;
  padding: 0;
  margin: 0;
}
@keyframes marquee {
  0% { transform: translateX(0); }
  100% { transform: translateX(-66.6%); }
}

</style>
            <footer id="footer">
                <div  ng-controller="notLoggedCtrl" class='row'>

                    <div class="col-md-12" >
                        <div class="marquee">
                            <div class="marquee__content">
                         
                                <ul class="f-menu">
                                        <li  ng-repeat=" countrz in marketwatch" ng-bind-html-unsafe="countrz.text"  class = 'cnbca_'>
                                           
                                            <span ng-if="countrz.market_change < 0" class ="our-red-color">{{countrz.market_company}} ⇨ {{countrz.market_vwap}} ( {{countrz.market_change}} ) </span>
                                            <span ng-if="countrz.market_change > 0 " class ="our-green-color">{{countrz.market_company}} ⇨ {{countrz.market_vwap}} ( {{countrz.market_change}} ) </span>
                                            <span ng-if="countrz.market_change == 0" class ="our-white-color">{{countrz.market_company}} ⇨ {{countrz.market_vwap}} ( {{countrz.market_change}} ) </span>                                       
                                        </li>
                                        <li  ng-repeat=" counds in marketwatchzse" ng-bind-html-unsafe="counds.text"  class = 'cnbca_'>
                                            <span class ="our-white-color">{{counds.Ticker}} ⇨ {{counds.Current_price}}  </span>                                       
                                        </li>                                    
                                </ul>

                            </div> 
                        </marquee>
                    </div>                
                </div>

            </footer>


            <!-- Page Loader -->
            <div class="page-loader">
                <div class="preloader pls-white">
                    <svg class="pl-circular" viewBox="25 25 50 50">
                        <circle class="plc-path" cx="50" cy="50" r="20" />
                    </svg>

                    <p style ="font-size:10px;">Connecting to C-TRADE...</p>
                </div>
            </div>

            <!-- Older IE warning message -->
            <!--[if lt IE 9]>
                <div class="ie-warning">
                    <h1 class="c-white">Warning!!</h1>
                    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                    <div class="iew-container">
                        <ul class="iew-download">
                            <li>
                                <a href="http://www.google.com/chrome/">
                                    <img src="img/browsers/chrome.png" alt="">
                                    <div>Chrome</div>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.mozilla.org/en-US/firefox/new/">
                                    <img src="img/browsers/firefox.png" alt="">
                                    <div>Firefox</div>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.opera.com">
                                    <img src="img/browsers/opera.png" alt="">
                                    <div>Opera</div>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.apple.com/safari/">
                                    <img src="img/browsers/safari.png" alt="">
                                    <div>Safari</div>
                                </a>
                            </li>
                            <li>
                                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                    <img src="img/browsers/ie.png" alt="">
                                    <div>IE (New)</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <p>Sorry for the inconvenience!</p>
                </div>   
            <![endif]-->
            
            <!-- Libs -->


            <script src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.33.3/es6-shim.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/systemjs/0.19.20/system-polyfills.js"></script>
            <script src="https://npmcdn.com/angular2/es6/dev/src/testing/shims_for_IE.js"></script>  
            <!-- Libs -->
            <script src="angular/js/angular.js"></script>
            <script src="angular/js/angular-idle.js"></script>
            <script src="js/ngStorage.min.js" ></script>  
            <script src="js/angular-chosen.min.js"></script>      
            <script src="angular/js/angular-sanitize.min.js"></script>
            <script src="js/d3.js"></script>
            <script src="angular/js/angular-resource.js"></script>
            <script src="angular/js/ngprogress.js"></script>
            <script src="js/ui-bootstrap-tpls-0.11.0.js"></script>
            <script src="js/nv.d3.js"></script>
            <script src="js/angularjs-nvd3-directives.js"></script>
            <script src="angular/js/angular-nvd3.min.js"></script>
            <script src="angular/js/angular-route.min.js"></script>
            <script src="angular/js/angular-animate.min.js" ></script>
            <script src="angular/js/toaster.js"></script>
            <script src="angular/js/angular-pageslide-directive.js"></script>
            <script src="vendors/ngDialog/js/ngDialog.min.js"></script>
            <script src="angular/app/ctrade.js"></script>
            <!-- Javascript Libraries -->
            <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
            <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
            <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>
            <script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
            <script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>



            <script src="js/app.js"></script>
        
        
    

    </body>
  </html>