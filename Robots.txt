User-agent: *
Disallow: /android/
Disallow: /angular/
Disallow: /api/
Disallow: /css/
Disallow: /docs/
Disallow: /fonts/
Disallow: /images/
Disallow: /img/
Disallow: /js/
Disallow: /less/
Disallow: /media/
Disallow: /paynow/
Disallow: /templates/
Disallow: /vendors/