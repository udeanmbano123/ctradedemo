<?php 
function  postSharesSell(
				$cds_no,
                $reference,
                $order_type,
                $company,
                $instrument,
                $security_dealer,
                $price,
                $qnty,
                $_name){
		require_once 'db_cds/DbConnectATS.php';
		$db_cds = new DbConnectATS();
		$conn_cds = $db_cds->connect();

		$sql  = "INSERT INTO Order_Live (
				           [OrderType]  
				           ,[Company]  
				           ,[SecurityType]  
				           ,[CDS_AC_No]  
				           ,[Broker_Code]  
				           ,[Shareholder]  
				           ,[ClientName]  
				           ,[OrderStatus]  
				           ,[Create_date] 
				           ,[Deal_Begin_Date] 
				           ,[Expiry_Date]  
				           ,[Quantity]  
				           ,[BasePrice]  
				           ,[OrderPref]  
				           ,[Marketboard]  
				           ,[TimeInForce]  
				           ,[OrderQualifier]  
				           ,[BrokerRef]  
				           ,[MaxPrice]  
				           ,[MiniPrice]  
				           ,[Flag_oldorder]  
				           ,[OrderNumber]  
				           ,[Currency]  
				           ,[FOK]  
							,[TotalShareHolding]  
							,[AvailableShares]  
							,[OrderAttribute]  
							,[ContraBrokerId] 
							,[Client_Type] 
							,[Tax]  				           
				           ,[Affirmation]) VALUES(

							'SELL'
							,'".strtoupper($company)."'
							,'EQUITY'
							,'".$cds_no."'
							,'".$security_dealer."'
							,'".$cds_no."'
							,'".$_name."'
							,'OPEN'
							, GETDATE()
							, GETDATE()
							,GETDATE()
							,'".$qnty."'
							,'".$price."'
							,'L'
							,'Normal Board'
							,'Day Order (DO)'
							,'None'
							,'".$security_dealer."".$reference."'
							,'0.00'
							,'0.00'
							,'0'
							,'".substr($company, 0, 4)."".$reference."'
							,'USD'
							,'0'
							,'0'
							,'0'
							,''
							, ''
							,''
							,'0'							
							,'1'

				           )
				    " ;

				    // echo $sql ; 

		$result=sqlsrv_query($conn_cds,$sql);

		$found = 0 ;
		if( $result === false ){
		     return "Error posting order" ;
		     die( print_r( sqlsrv_errors(), true));
		}else{
			return "Order successfully posted" ;
		}

		sqlsrv_free_stmt( $result);
		sqlsrv_close( $conn_cds);

}


?>