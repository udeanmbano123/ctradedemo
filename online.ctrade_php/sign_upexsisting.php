<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C-TRADE PRE CREATE ACCOUNT</title>

        <!-- Vendor CSS -->
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
        <link href="vendors/bower_components/google-material-color/dist/palette.css" rel="stylesheet">

        <link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="vendors/bower_components/chosen/chosen.min.css" rel="stylesheet">
        <link href="vendors/summernote/dist/summernote.css" rel="stylesheet">
        <link rel="icon" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon-150x150.png" sizes="32x32" />
        <link rel="icon" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="https://ctrade.co.zw/wp-content/uploads/2018/02/favicon.png" />
        <!-- CSS -->
        <link href="css/style.css" rel="stylesheet">
        <link href="css/app.min.1.css" rel="stylesheet">
        <link href="css/app.min.2.css" rel="stylesheet">
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="css/buttons.dataTables.min.css" rel="stylesheet">
        
        <!-- Following CSS are used only for the Demp purposes thus you can remove this anytime. -->
        <style type="text/css">
            .toggle-switch .ts-label {
                min-width: 130px;
            }
            .error{
                color: #F44336 ; 
            }
            form{
                margin: 20px 0;
            }
            form input, button{
                padding: 5px;
            }
            table{
                width: 100%;
                margin-bottom: 20px;
                border-collapse: collapse;
            }
            table, th, td{
                border: 1px solid #cdcdcd;
            }
            table th, table td{
                padding: 10px;
                text-align: left;
            }

        </style>
            <!-- CSS -->
            <link href="css/bootstrap.css" rel="stylesheet">
            <link href="css/style.css" rel="stylesheet">
            <link href="font-awesome/css/font-awesome.css" rel="stylesheet" >
            <link href="css/socialize-bookmarks.css" rel="stylesheet">
            <link href="js/fancybox/source/jquery.fancybox.css?v=2.1.4" rel="stylesheet">
            <link href="check_radio/skins/square/aero.css" rel="stylesheet">
            <!-- Toggle Switch -->
            <link rel="stylesheet" href="css/jquery.switch.css">
            <!-- Owl Carousel Assets -->
            <link href="css/owl.carousel.css" rel="stylesheet">
            <link href="css/owl.theme.css" rel="stylesheet">
            <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
            <!-- Jquery -->
            <script src="js/jquery-1.11.1.js"></script>
            <script src="js/jquery-ui-1.8.22.min.js"></script>
            <!-- Wizard-->
            <script src="js/jquery.wizard.js"></script>
            <!-- Radio and checkbox styles -->
            <script src="check_radio/jquery.icheck.js"></script>
            <!-- HTML5 and CSS3-in older browsers-->
            <script src="js/modernizr.custom.17475.js"></script>
            <!-- Support media queries for IE8 -->
            <script src="js/respond.min.js"></script>        
        <script>

var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];    
function ValidateSingleInput(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
		if(oInput.files[0].size > 5 * 1024 * 1024){
			 alert("Max file size is 5MB.");
   oInput.value = "";
                return false;
		}
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                oInput.value = "";
                return false;
            }
        }
    }
	ValidateSize(oInput);
    return true;
}
var _validFileExtensions2 = [".jpg", ".jpeg", ".bmp", ".gif", ".png",".doc",".docx",".pdf"]; 
function ValidateSingleInput2(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
		
		if(oInput.files[0].size > 5 * 1024 * 1024){
			 alert("Max file size is 5MB.");
   oInput.value = "";
                return false;
		}
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions2.length; j++) {
                var sCurExtension = _validFileExtensions2[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions2.join(", "));
                oInput.value = "";
                return false;
            }
        }
    }

    return true;
}
</script>	
		<?php
		$cdsnumber=$_GET["cds"];
	
  $responses ="https://demo.ctrade.co.zw/mobileapi/PreshareHolder?cdsnumber=".$cdsnumber;

$ch = curl_init();
 
        // set url
        curl_setopt($ch, CURLOPT_URL,$responses);
 
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false) ;
 		//curl_setopt($ch,CURLOPT_POST,count(8));
        //curl_setopt($ch,CURLOPT_POSTFIELDS,);
        // $output contains the output string
        $output = curl_exec($ch);

  
        // close curl resource to free up system resources
        curl_close($ch); 
     
   
	$decoded = json_decode($output);
$comments = $decoded->predetails;
$Shareholder=$_GET["cds"];
$Name="";
$Shares="";
$Gross="";
$Tax="";
$offer_shares="";
$Nett="";
$add_1="";
$add_2="";
$add_3="";
$add_4="";
$add_5="";
$Country="";
$Industry="";
$Tax_Code="";
$Tax_Rate="";
$Bank="";
$Bank_Branch="";
$Bank_Ac="";
foreach($comments as $comment){
 
$Name=$comment->Name;
$Shares=$comment->Shares;
$Gross=$comment->Gross;
$Tax=$comment->Tax;
$offer_shares=$comment->offer_shares;
$Nett=$comment->Nett;
$add_1=$comment->add_1;
$add_2=$comment->add_2;
$add_3=$comment->add_3;
$add_4=$comment->add_4;
$add_5=$comment->add_5;
$Country=$comment->Country;
$Industry=$comment->Industry;
$Tax_Code=$comment->Tax_Code;
$Tax_Rate=$comment->Tax_Rate;
$Bank=$comment->Bank;
$Bank_Branch=$comment->Bank_Branch;
$Bank_Ac=$comment->Bank_Ac;//do something with it
 
}
      $fulladdr=  $add_1 . ' ' . $add_2. ' ' .$add_3. ' ' .$add_4. ' ' .$add_5;   
	?>
</head>


    <body > 

<header id="header" class="clearfix">
            <ul class="h-inner">
                <li class="hidden-xs">
                    <a href="https://ctrade.co.zw/online_v2/" class="p-l-10">
                        <img src="img/demo/logo.png" style="width: 34%;" alt="">
                    </a>
                </li>

                <li class="pull-right">
                    <ul class="hi-menu">


                        <li class="dropdown" >
                            <a data-toggle="dropdown" href=""><span class="him-label" style ="color:#fff;">Guest</span></a>
                        </li>
                    </ul>
                </li>
            </ul>

        </header>

        
        <section id="main">

            <section id="content" >
                <div class="container" style="margin-top: 100px;">

                   <div id="survey_container">

                        <div id="top-wizard">
                          <strong>Progress <span id="location"></span></strong>
                          <div id="progressbar"></div>
                          <div class="shadow"></div>
                        </div>  

                        <div class="row"> 

                            <div class="col-sm-12 col-xs-6" >

                                        <!-- Form -->
                                        <div class="card" id ="place_order">
                                            
                                            <div class="card-header ch-alt">
                                                <h1 style="text-align:left;">
                                                    Create Account </h1>
                                                <h2>
                                                    <small>Create a cds account </small>
                                                </h2>
                                            </div>

                                            <div class="card-body card-padding">

                                                <form name  = "invoice" id="wrappeddddd" action = "" method = "POST" enctype="multipart/form-data">
                                                    <div class="row">
                                                        <fieldset>
                                                            
                                                          <legend class="c-blue"> Create account as </legend>
                                                           <div class="row">
                                                                <div class="col-sm-6  ">
                                                                    <label class="c-white" >Individual</label>
                                                                        <input type="radio" name="account_type_new" value="Individual" checked="true"  onclick="show1()"> <br>
                                                                </div>  
                                                                <div class="col-sm-6  ">
                                                                    <label class="c-white" >Company</label>
                                                                        <input type="radio" name="account_type_new" value="Company"  onclick="show2()">
                                                                </div>  
                                                          </div>
                                                           
                                                        </fieldset>
                                                      <div id="middle-wizard" class="individual">

                                                         
                                                            <div class="step">                                                    
                                                                <input type ="hidden" name= "acc_type" value="i" >
                                                                <input type ="hidden" name= "source" value="web" >
																<input type ="hidden" name= "cds" id="cds" value="<?php echo $cdsnumber; ?>" >
																<input type ="hidden" name= "nett" id="nett" value="<?php echo  $Nett; ?>" >
                                                                <div class="col-sm-12">
                                                                    <div class="row">
                                                                        <div class="col-sm-4  ">
                                                                            <label class="c-blue" >Title</label>
                                                                            <select name  = "my_title" class="selectpicker required" >
                                                                                <option value = "Mr">Mr</option>
                                                                                <option value = "Mrs">Mrs</option>
                                                                                <option value = "Mrs">Miss</option>
                                                                                <option value = "Mrs">Ms.</option>
                                                                                <option value = "Mrs">Sir.</option>
                                                                                <option value = "Mrs">Esq.</option>
                                                                                <option value = "Mrs">Rev.</option>
                                                                                <option value = "Mrs">Dr.</option>
                                                                                <option value = "Mrs">Prof.</option>
                                                                            </select>
                                                                        </div>  


                                                                        <div class="col-sm-4 m-b-20">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Forenames <br></label>
                                                                                <input type="text" value="<?php echo $Name; ?>" name  = "fore_name" id = "fore_name" value="<?php $Name; ?>" class="form-control required letterswithbasicpunc"  >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4 m-b-20">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Surname <br></label>
                                                                                <input type="text" value="<?php echo $Name; ?>" name  = "surname" id = "surname" class="form-control required letterswithbasicpunc" >
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                </div> 

                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Date of Birth <br></label>
                                                                                <input type="text" name="dob" id="dob" data-date-start-date="-120y" data-date-end-date="-18y"  class="form-control required date-picker valid" placeholder="Click here..." aria-required="true" aria-invalid="false">
                                                                                <!-- <input type="text" name  = "" id = "my_price" class="form-control "  placeholder="Date of Birth"> -->
                                                                            </div>
                                                                        </div> 
                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Gender </label><br>
                                                                                <input type="radio" class ="required" name="gender" value="Male"  >
                                                                                <label for="compete_no">Male</label>

                                                                                <input type="radio" class ="required" name="gender" value="Female">
                                                                                <label for="compete_yes">Female</label>
                                                                            </div>
                                                                        </div>  

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">National ID<br></label>
                                                                                <input type="text" name  = "nat_id" id = "nat_id" class="form-control required"  placeholder="National ID">
                                                                            </div>
                                                                        </div>    
                                                                
                                                                    </div>    
                                                                </div> 
              

                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Password<br></label>
                                                                                <input type="password" name  = "my_pass" id = "pass" class="form-control required"  placeholder="Password">
                                                                            </div>
                                                                        </div> 
                                                                        <div class="col-sm-8  ">

                                                                        </div> 
               
                                                                    </div>     
                                                                </div>   

                                                                <div class="col-sm-12">
                                                                    <!-- /Row -->   
                                                                    <div class="row">
                                                                       <div class="col-md-4 col-md-offset-4">
                                                                          <ul class="data-list" id="terms">
                                                                             <li>
                                                                                <strong>Do you accept <a href="../docs/C-Trade Mobile  Online  App - Terms and Conditions v1.pdf" target="_blank" > terms and conditions</a> ?</strong>
                                                                                <label class="switch-light switch-ios ">
                                                                                <input type="checkbox" name="terms" class="form-control  required fix_ie8" value="yes">
                                                                                <span>
                                                                                <span class="ie8_hide">No</span>
                                                                                <span>Yes</span>
                                                                                </span>
                                                                                <a></a>
                                                                                </label>
                                                                             </li>
                                                                          </ul>
                                                                       </div>
                                                                    </div>                                                            
                                                                </div>
                                                            </div>

                                                            <div class="step"> 
                                                                <div class="col-sm-12">
                                                                    Location <br>
                                                                    <div class="row">

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Address-Home<br></label>
                                                                                <input type="text" value="<?php echo  $fulladdr; ?>" name  = "add_home" id = "add" class="form-control required"  placeholder="Address-Home">
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Address-Business <br></label>
                                                                                <input type="text"  value="<?php echo  $fulladdr; ?>" name  = "add_bus" id = "addr" class="form-control required"  placeholder="Address-Business1">
                                                                            </div>
                                                                        </div>     
                                                                    </div>     
                                                                </div>  


                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-4  ">

                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Country <br></label>
                                                                                <select name  = "country" class="selectpicker required" id="cont" data-live-search="true">
                                                                                  <?php include("contt.php") ; ?>
                                                                                </select>                                                                            
                                                                                <!-- <input type="text" name  = "" id = "my_price1" class="form-control "  placeholder="Country"> -->
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">City<br></label>
                                                                                <input type="text" name  = "city" id = "city" class="form-control required"  placeholder="City">
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Mobile Number <br></label>
                                                                                <input type="text" name  = "cell_num" id = "mobile" class="form-control required"  placeholder="Mobile Number ">
                                                                            </div>
                                                                        </div>

                                                                    </div>     
                                                                </div>     

                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Telephone<br></label>
                                                                                <input type="text" name  = "telphone" id = "tele" class="form-control required"  placeholder="Telephone">
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Email <br></label>
                                                                                <input type="email" name  = "my_email" id = "email" class="form-control required"  placeholder="Email">
                                                                            </div>
                                                                        </div>     
                                                                    </div>     
                                                                </div>  
                                                            </div>

                                                            
                                                            <div class="step">  

                                                                <div class="col-sm-12">
                                                                    Banking Details <br>
                                                                    <div class="row">

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Bank Name<br></label>
                                                                                <select name  = "bank_name" class="selectpicker required" data-live-search="true"   id ="bank_name">
                                                                                 <?php include("bankss.php") ; ?>
                                                                                </select>     

                                                                                <!-- <input type="text" name  = "" id = "my_price1" class="form-control "  placeholder="Bank Name"> -->
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Branch <br></label>

                                                                                <select name  = "branch" class="selectpicker required" id ="branch">
                                                                               <?php include("getBranchh.php") ; ?> 
																				<option value = "0">  Please Select bank first  </option>
                                                                                
																				</select>                                                                                 
                                                                                <!-- <input type="text" name  = "" id = "" class="form-control required"  placeholder="Branch"> -->
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Account Number <br></label>
                                                                                <input type="text" value="<?php echo  $Bank_Ac; ?>" name  = "acc_num" id = "acc_num" class="form-control required"  placeholder="Account Number">
                                                                                 <input type ="hidden" name  = "stock_brocker" value = "WESTS" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                        
                                                                        </div>


                                                                    </div>     
                                                                </div>     

                                                                <div class="col-sm-12">
                                                                    <div class="row">



                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Custodian<br></label>
                                                                                    <select name  = "custodian" class="selectpicker required" data-live-search="true" id ="custodian">
                                                                                        <?php include("getCustodian.php") ; ?>
                                                                                    </select>                                                                    
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12  ">
                                                                               Security Questions
                                                                        </div>
                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue"> 1.  What is your favorite book?  <br></label>
                                                                                <input type="text" name  = "qsn1" id = "qsn1" class="form-control"  placeholder="">
                                                                                 
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">2.  What is your mother’s maiden name?<br></label>
                                                                                <input type="text" name  = "qsn2" id = "qsn2" class="form-control"  placeholder="">
                                                                                
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">3.  What is the name of the road you grew up in?  <br></label>
                                                                                <input type="text" name  = "qsn3" id = "qsn3" class="form-control"  placeholder="">
                                                                                 
                                                                            </div>
                                                                        </div>

					
																		

                                                                        <div class="col-sm-4  ">

                                                                        </div>

                                                                    </div>     
                                                                </div>     

		         
													<script type="text/javascript">
function Check() {
    if (document.getElementById('xchange').checked) {
        document.getElementById('agent').style.display = 'block';
    } 
    else {
        document.getElementById('agent').style.display = 'none';

   }
}
function myFunction(val) {
 document.getElementById("txt").value=val;
}
</script>
																		
                                                                       <div class="col-sm-4  ">
                                                                           <div class="form-group fg-line">
																		       <label class="c-blue"><h6>Where you referred to C-Trade by an Agent?</h6></label>

                                                                      YES<input type="radio" onclick="Check();" name="xchange" value ="YES" id="xchange" >

                                                                        NO<input type="radio" onclick="Check();"  name="xchange" value="NO" id="xchange">
                 
				 
                                                                                <input type="text" style="display:none"  name  = "agent" id="agent" class="form-control required"  placeholder="Agent Code" onchange="myFunction(this.value)">
                                                                                 
                                                                            </div>
                                                                        </div> 
												
												
                                                                <div class="clearfix"></div>
<div class="col-sm-12  ">
					  									       <label class="c-blue"><h6>KYC Lite</h6></label>
                                                                           <div class="form-group fg-line">
	   Upload Profile Photo:<i>Max file size 5mb</i>
    <input type="file" name="Profile" onchange="ValidateSingleInput(this);">
	 Upload National ID:<i>Max file size 5mb</i>
	<input type="file" name="NationalIdd"   onchange="ValidateSingleInput2(this);">
	 Upload Proof of Residence:<i>Max file size 5mb</i>
	<input type="file" name="ProofOfResidence"   onchange="ValidateSingleInput2(this);">
  

                                  
                                                                              
                                                                            </div>
                                                                        </div> 
												<div class="clearfix"></div>
                                                            </div>
                                      
                                                             <!-- end step-->
                                                             <div class="submit step" id="complete">
													
                                                                <i class="icon-check"></i>
                                                                <h3>Your application is now ready for submission. </h3>
                                                                <button type="submit" name="process" class="submit">Submit the registration details</button>                                                             
                                                             </div>
                                                             <!-- end submit step -->
                                                             <!-- </div> -->
                                                             <!-- end middle-wizard -->
                                                             <div id="bottom-wizard">
                                                                <button type="button" name="backward" class="backward">Back</button>
                                                                <button type="button" name="forward" class="forward">Next</button>

                                                             </div>
                                                             <!-- end bottom-wizard -->


                                                      </div>
                                                </form>
                                                <form name  = "company" id="wrappeddddd" action = "func/create_accountexsist.php" method = "POST" enctype="multipart/form-data">
                                                      <div id="middle-wizard" class="company" style="display: none;border-top: #fff 1px solid;">

                                                                <div class="col-sm-12">
                                                                    <input type ="hidden" name= "acc_type" value="c" >
                                                                    <input type ="hidden" name= "my_title" value="" >
                                                                    <input type ="hidden" name= "fore_name" value="" >
                                                                    <input type ="hidden" name= "gender" value="N" >
                                                          	<input type ="hidden" name= "cds" id="cds" value="<?php  echo  $cdsnumber; ?>" >
																<input type ="hidden" name= "nett" id="nett" value="<?php  echo  $Nett; ?>" >
                                                                    <input type ="hidden" name= "source" value="web" >       
                                                                </div>
                                                                
                                                                <div class="col-sm-12">
                                                                    <div class="row">
                                                                        <div class="col-sm-6 m-b-20">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Company Name <br></label>
                                                                                <input type="text" value="<?php echo  $Name; ?>" name  = "surname" id = "surname_" class="form-control required letterswithbasicpunc"  placeholder="Company Name">
                                                                            </div>
                                                                        </div>    

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Certificate of Incorporation Number<br></label>
                                                                                <input type="text" name  = "nat_id" id = "nat_id_" class="form-control required"  placeholder="Certificate of Incorporation Number">
                                                                            </div>
                                                                        </div>    

                                                                    </div>
                                                                </div> 

                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Date of incoporation <br></label>
                                                                                <input type="text" name="dob" id="dob_" data-date-start-date="-120y" data-date-end-date="-18y"  class="form-control required date-picker valid" placeholder="Click here..." aria-required="true" aria-invalid="false">
                                                                                <!-- <input type="text" name  = "" id = "my_price" class="form-control "  placeholder="Date of Birth"> -->
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-8  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Custodian<br></label>
                                                                                    <select name  = "custodian" class="selectpicker required" data-live-search="true" id ="custodian_">
                                                                                        <?php include("getCustodian.php") ; ?>
                                                                                    </select>                                                                    
                                                                            </div>
                                                                        </div>

                                                                
                                                                    </div>    
                                                                </div> 
                                                                <div class="col-sm-12">
                                                                    Location <br>
                                                                    <div class="row">

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Physical Address<br></label>
                                                                                <input type="text" value="<?php  echo $fulladdr; ?>" name  = "add_home" id = "add_" class="form-control required"  placeholder="Address-Home">
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Postal Address <br></label>
                                                                                <input type="text" value="<?php echo  $fulladdr; ?>" name  = "add_bus" id = "addr_" class="form-control required"  placeholder="Address-Business1">
                                                                            </div>
                                                                        </div>     
                                                                    </div>     
                                                                </div>  


                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-4  ">

                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Country of Incoparation <br></label>
                                                                                <select name= "country" class="selectpicker required" id="cont_" data-live-search="true">
                                                                                 <?php include("contt.php") ; ?>
                                                                                </select>                                                                            
                                                                                <!-- <input type="text" name  = "" id = "my_price1" class="form-control "  placeholder="Country"> -->
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">City<br></label>
                                                                                <input type="text" name  = "city" id = "city_" class="form-control required"  placeholder="City">
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-4  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Phone Number<br></label>
                                                                                <input type="text" name  = "cell_num" id = "cell_num_" class="form-control required"  placeholder="Phone Number">
                                                                            </div>
                                                                        </div> 


                                                                    </div>     
                                                                </div>     

                                                                <div class="col-sm-12">
                                                                    <div class="row">

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Telephone<br></label>
                                                                                <input type="text" name  = "telphone" id = "tele_" class="form-control required"  placeholder="Telephone">
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Company Email <br></label>
                                                                                <input type="email" name  = "my_email" id = "email_" class="form-control required"  placeholder="Email">
                                                                            </div>
                                                                        </div>     
                                                                    </div>     
                                                                </div>  
                                                            

                                                                <div class="col-sm-12">
                                                                    Banking Details <br>
                                                                    <div class="row">

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Bank Name<br></label>
                                                                                <select name  = "bank_name" class="selectpicker required" data-live-search="true"   id ="bank_name">
                                                                                   <?php include("bankss.php") ; ?>
                                                                                   </select>     

                                                                                <!-- <input type="text" name  = "" id = "my_price1" class="form-control "  placeholder="Bank Name"> -->
                                                                            </div>
                                                                        </div> 

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Branch <br></label>

                                                                                <select name  = "branch" class="selectpicker required" id ="branch_comp">
                                                                                   <?php include("getBranchh.php") ; ?> 
																				<option value = "0">  Please Select bank first  </option>
                                                                                </select>                                                                                 
                                                                                <!-- <input type="text" name  = "" id = "" class="form-control required"  placeholder="Branch"> -->
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-6  ">
                                                                            <div class="form-group fg-line">
                                                                                <label  class="c-blue">Account Number <br></label>
                                                                                <input type="text"  value="<?php echo  $Bank_Ac; ?>" name  = "acc_num" id = "acc_num_" class="form-control required"  placeholder="Account Number">
                                                                                 <input type ="hidden" name  = "stock_brocker" value = "WESTS" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                            <div class="form-group fg-line">
                                                                                 <label  class="c-blue">Password <br></label>
                                                                                <input type="password" name  = "my_pass" id = "my_pass__" class="form-control required"  placeholder="Password">
                                                                            </div>                                                                        
                                                                        </div>


                                                                    </div>     
                                                                </div>     

                                                                <div class="col-sm-12">
                                                                    Add Representatives <br>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <form>
                                                                                <div class="row">
                                                                                <div class="col-sm-2  ">
                                                                                <input type="text" id = "my_name" class="form-control required"  placeholder="Name">
                                                                                <input type="hidden" name="comp_rep_ttl" id = "my_bal_users" value = "0">
                                                                                </div>
                                                                                <div class="col-sm-2  ">
                                                                                <input type="text" id = "my_email" class="form-control required"  placeholder="Email">
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                <input type="text" id = "my_phone" class="form-control required"  placeholder="Phone">
                                                                                </div>
                                                                                <div class="col-sm-2  ">
                                                                                <input type="text" id = "my_pass_no" class="form-control required"  placeholder="Password">
                                                                                </div>
                                                                                <div class="col-sm-2  ">
                                                                                    Permission<br>
                                                                                Initiator <input type="checkbox" id = "my_perm_1" value ="yes" placeholder="Initiator">
                                                                                Authorizer<input type="checkbox" id = "my_perm_2" value ="yes" placeholder="Authorizer">
                                                                                </div>
                                                                                <div class="col-sm-2  ">
                                                                                <input type="button" class="add-row kabhatani" value="[Add Representatives]">
                                                                                </div>
                                                                                </div>
                                                                            </form>
                                                                            <table>
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="c-blue" >Select</th>
                                                                                        <th class="c-blue" >Name</th>
                                                                                        <th class="c-blue" >Email</th>
                                                                                        <th class="c-blue" >Phone</th>
                                                                                        <th class="c-blue" >Password</th>
                                                                                        <th class="c-blue" >Initiator</th>
                                                                                        <th class="c-blue" >Authorizer</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                </tbody>
                                                                            </table>
                                                                            <button type="button"  class="delete-row kabhatani">[Delete Representative]</button>
                                                                        </div> 
                                                                    </div>     
                                                                </div>     
 	<script type="text/javascript">
function Check2() {
    if (document.getElementById('xchange2').checked) {
        document.getElementById('agent2').style.display = 'block';
    } 
    else {
        document.getElementById('agent2').style.display = 'none';

   }
}

</script>
																		
                                                                       <div class="col-sm-4  ">
                                                                           <div class="form-group fg-line">
																		       <label class="c-blue"><h6>Where you referred to C-Trade by an Agent?</h6></label>

                                                                      YES<input type="radio" onclick="Check2();" name="xchange2" value ="YES" id="xchange2" >

                                                                        NO<input type="radio" onclick="Check2();"  name="xchange2" value="NO" id="xchange2">
                 
				 
                                                                                <input type="text" style="display:none"  name  = "agent2" id="agent2" class="form-control required"  placeholder="Agent Code" onchange="myFunction(this.value)">
                                                                                 
                                                                            </div>
                                                                        </div> 
												
												
                                                                <div class="clearfix"></div>
<div class="col-sm-12  ">
					  									       <label class="c-blue"><h6>KYC Lite</h6></label>
                                                                           <div class="form-group fg-line">
	   Upload Profile Photo:<i>Max file size 5mb</i>
    <input type="file" name="Profile2" onchange="ValidateSingleInput(this);">
	 Upload National ID:<i>Max file size 5mb</i>
	<input type="file" name="NationalIdd2"   onchange="ValidateSingleInput2(this);">
	 Upload Proof of Residence:<i>Max file size 5mb</i>
	<input type="file" name="ProofOfResidence2"   onchange="ValidateSingleInput2(this);">
  

                                  
                                                                              
                                                                            </div>
                                                                        </div> 
												<div class="clearfix"></div>

                                                                <div class="clearfix"></div>                                                                

                                                                <div class="col-sm-12">
                                                                    <!-- /Row -->   
                                                                    <div class="row">
                                                                       <div class="col-md-4 col-md-offset-4">
                                                                          <ul class="data-list" id="terms">
                                                                             <li>
                                                                                <strong>Do you accept <a href="../docs/C-Trade Mobile  Online  App - Terms and Conditions v1.pdf" target="_blank" > terms and conditions</a> ?</strong>
                                                                                <label class="switch-light switch-ios ">
                                                                                <input type="checkbox" name="terms" class="form-control  required fix_ie8" value="yes">
                                                                                <span>
                                                                                <span class="ie8_hide">No</span>
                                                                                <span>Yes</span>
                                                                                </span>
                                                                                <a></a>
                                                                                </label>
                                                                             </li>
                                                                          </ul>
                                                                       </div>
                                                                    </div>                                                            
                                                                </div>

                                                                <div  class="col-sm-12" >
                                                                    <div class="" id="complete">
                                                                        <button type="submit" name="processed" class="">Submit the registration details</button>                                                             
                                                                     </div>
                                                                </div>

                                                                <div class="clearfix"></div>


                                                    </div>
                                                <input type="text" style="display:none" name="txt" id="txt"  class="form-control required">

                                            </div>

                                        </div>

                            </div>

                        </div>


                    </div>
                </div>
            </section>

<div class="modalz"></div>
           
<div id="modalConfirmYesNo" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" 

                class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="lblTitleConfirmYesNo" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p id="lblMsgConfirmYesNo"></p>
            </div>
            <div class="modal-footer">
                <button id="btnYesConfirmYesNo" 

                type="button" class="btn btn-primary">Proceed to login</button>
                <button id="btnNoConfirmYesNo" 

                type="button" class="btn btn-default">New Registration</button>
            </div>
        </div>
    </div>
</div>

        </section>

       <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        
        <script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>

        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>
        <script src="vendors/summernote/dist/summernote-updated.min.js"></script>
        
        <script src="vendors/bower_components/chosen/chosen.jquery.min.js"></script>
        <script src="vendors/fileinput/fileinput.min.js"></script>
        <script src="vendors/input-mask/input-mask.min.js"></script>
        <script src="vendors/farbtastic/farbtastic.min.js"></script>
        <script src="js/jqClock.min.js"></script>

        <script src="js/functions.js"></script>
        <script src="js/actions.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.buttons.min.js"></script>
        <script src="js/buttons.print.min.js"></script>
        <script src="js/demo.js"></script>
        <script src="js/jquery.validate.js"></script>
        <script src="js/additional-methods.js"></script>
        <script type="text/javascript">
                    $(document).ready(function(){
                        $(".add-row").click(function(){
                            var name = $("#my_name").val();
                            var email = $("#my_email").val();
                            var pass = $("#my_pass_no").val();
                            var phone = $("#my_phone").val();
                            var my_users = $("#my_bal_users").val();
                            var initiator = "no" ; 
                            var authorizer = "no" ; 
                            if ($('#my_perm_1').is(':checked')) {
                                initiator = "yes";
                            } else {
                                initiator = "no" ;
                            }                           
                            if ($('#my_perm_2').is(':checked')) {
                                authorizer = "yes" ; 
                            } else {
                                authorizer = "no" ; 
                            }
                            $("#my_bal_users").val(parseInt(my_users) + 1 );
                            var markup = "<tr><td><input type ='hidden' name= 'comp_rep_name_"+$("#my_bal_users").val()+"' value='"+name+"' ><input type ='hidden' name= 'comp_rep_email_"+$("#my_bal_users").val()+"' value='"+email+"' ><input type ='hidden' name= 'comp_rep_phone_"+$("#my_bal_users").val()+"' value='"+phone+"' ><input type ='hidden' name= 'comp_rep_pass_"+$("#my_bal_users").val()+"' value='"+pass+"' ><input type ='hidden' name= 'comp_rep_initiator_"+$("#my_bal_users").val()+"' value='"+initiator+"' ><input type ='hidden' name= 'comp_rep_authorizer_"+$("#my_bal_users").val()+"' value='"+authorizer+"' ><input type='checkbox' name='record'></td><td>" + name + "</td><td>" + email + "</td><td>" + phone + "</td><td>" + pass + "</td><td>" + initiator + "</td><td>" + authorizer + "</td></tr>";
                            $("table tbody").append(markup);
                            $("#my_name").val("");
                            $("#my_email").val("");
                            $("#my_pass_no").val("");
                            $("#my_phone").val("");
                            $("#my_bal_users").val("");                            
                        });
                        
                        // Find and remove selected table rows
                        $(".delete-row").click(function(){
                            $("table tbody").find('input[name="record"]').each(function(){
                                if($(this).is(":checked")){
                                    $(this).parents("tr").remove();
                                    var my_users = $("#my_bal_users").val();
                                    $("#my_bal_users").val(parseInt(my_users) - 1);

                                }
                            });
                        });
                    });            
                    function show2(){
                        $('.individual').hide() ;
                        $('.company').show() ;
                    }
                    function show1(){
                        $('.company').hide() ;
                        $('.individual').show() ;
                    }            
            function AsyncConfirmYesNo(title, msg, yesFn, noFn) {
                var $confirm = $("#modalConfirmYesNo");
                $confirm.modal('show');
                $("#lblTitleConfirmYesNo").html(title);
                $("#lblMsgConfirmYesNo").html(msg);
                $("#btnYesConfirmYesNo").off('click').click(function () {
                    yesFn();
                    $confirm.modal("hide");
                });
                $("#btnNoConfirmYesNo").off('click').click(function () {
                    noFn();
                    $confirm.modal("hide");
                });
            }
function ShowConfirmYesNo(message) {
    AsyncConfirmYesNo(
        "Account Creation",
        message,
        MyYesFunction,
        MyNoFunction
    );
}            

function MyYesFunction() {
    alert("Login to trade now");
    // $("#lblTestResult").html("You are about to exit ");
    window.location.href = "http://demo.ctrade.co.zw/ctrade_v2/";
}
function MyNoFunction() {
    alert("Create new account");
    window.location.href = "http://demo.ctrade.co.zw/ctrade_v2/";
    // $("#lblTestResult").html("You are not hungry");
}
         $(document).ready(function(){  

<?php
if(isset($_GET['message'])){
    echo "ShowConfirmYesNo('".htmlspecialchars($_GET['message'], ENT_QUOTES, 'UTF-8')."') ;" ; 
}
?>

                        $body = $("body");

                        $(document).on({
                             ajaxStart: function() { $body.addClass("loading");    },
                             ajaxStop: function() { $body.removeClass("loading"); }    
                        }); 

                        // $("#post-btn-nooo").click(function(){        
                        //     $.post("func/create_acc.php", $("#wrappeddddd").serialize(), function(data) {
                        //         // alert(data);
                        //         ShowConfirmYesNo(data) ;
                        //     });
                        // });

                        var val_yeduw ;
                        var text ;  
                        $('.selectpicker').selectpicker({
                          size: 10
                        });
                        $('select[name="bank_name"]').change(function() {
                            val_yeduw = $(this).val() ; 
                            //$('select[name="branch"]').selectpicker('refresh');
                            // alert(val_yeduw) ; 
                            $("#branch option").each(function() {
                                $(this).remove();
                            });
                            $('select[name="branch"]').selectpicker('refresh');
                            $.ajax({
                                type: "GET",
                                url: "getBranch.php",
                                /*or any server side url which generate citylist for rails
                                         "/states/city_list"or<%= city_list_states_path%> */
                                data: "bank="+val_yeduw,
                               /*here we are passing state_id to server based on that on
                                     he can find out  all the city related to that states */
                                success: function(html){
                                    /*html content response of city_list.xxx */
                                    $("#branch").html(html);
                                    $("#branch_comp").html(html);
                                    $('select[name="branch"]').selectpicker('refresh');
                                }
                            });

                        });


                     
            
            });     
        </script>
            <script src="js/jquery.tweet.min.js"></script>
            <script src="js/jquery.bxslider.min.js"></script>
            <script src="js/quantity-bt.js"></script>

            <script src="js/retina.js"></script>
            <script src="js/owl.carousel.min.js"></script>
            <script src="js/function_ss.js"></script>
            <script src="js/function_f.js"></script>
            <!-- FANCYBOX -->
            <script  src="js/fancybox/source/jquery.fancybox.pack.js?v=2.1.4" type="text/javascript"></script> 
            <script src="js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5" type="text/javascript"></script> 
            <script src="js/fancy_func.js" type="text/javascript"></script> 

    </body>
  
</html>