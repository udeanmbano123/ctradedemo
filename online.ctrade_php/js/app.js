jQuery(document).ready(function () {
    jQuery('body').on('click', '[data-ma-action]', function (e) {
        e.preventDefault();

        var jQuerythis = jQuery(this);
        var action = jQuery(this).data('ma-action');

        switch (action) {

            /*-------------------------------------------
                Sidebar & Chat Open/Close
            ---------------------------------------------*/
            case 'sidebar-open':
                var target = jQuerythis.data('ma-target');
                var backdrop = '<div data-ma-action="sidebar-close" class="ma-backdrop" />';

                jQuery('body').addClass('sidebar-toggled');
                jQuery('#header, #header-alt, #main').append(backdrop);
                jQuerythis.addClass('toggled');
                jQuery(target).addClass('toggled');

                break;

            case 'sidebar-close':
                jQuery('body').removeClass('sidebar-toggled');
                jQuery('.ma-backdrop').remove();
                jQuery('.sidebar, .ma-trigger').removeClass('toggled')

                break;


            /*-------------------------------------------
                Mainmenu Submenu Toggle
            ---------------------------------------------*/
            case 'submenu-toggle':
                jQuerythis.next().slideToggle(200);
                jQuerythis.parent().toggleClass('toggled');

                break;


            /*-------------------------------------------
                Top Search Open/Close
            ---------------------------------------------*/
            //Open
            case 'search-open':
                jQuery('#header').addClass('search-toggled');
                jQuery('#top-search-wrap input').focus();

                break;

            //Close
            case 'search-close':
                jQuery('#header').removeClass('search-toggled');

                break;


            /*-------------------------------------------
                Header Notification Clear
            ---------------------------------------------*/
            case 'clear-notification':
                var x = jQuerythis.closest('.list-group');
                var y = x.find('.list-group-item');
                var z = y.size();

                jQuerythis.parent().fadeOut();

                x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
                x.find('.grid-loading').fadeIn(1500);


                var w = 0;
                y.each(function(){
                    var z = jQuery(this);
                    setTimeout(function(){
                        z.addClass('animated fadeOutRightBig').delay(1000).queue(function(){
                            z.remove();
                        });
                    }, w+=150);
                })

                //Popup empty message
                setTimeout(function(){
                    jQuery('#notifications').addClass('empty');
                }, (z*150)+200);

                break;


            /*-------------------------------------------
                Fullscreen Browsing
            ---------------------------------------------*/
            case 'fullscreen':
                //Launch
                function launchIntoFullscreen(element) {
                    if(element.requestFullscreen) {
                        element.requestFullscreen();
                    } else if(element.mozRequestFullScreen) {
                        element.mozRequestFullScreen();
                    } else if(element.webkitRequestFullscreen) {
                        element.webkitRequestFullscreen();
                    } else if(element.msRequestFullscreen) {
                        element.msRequestFullscreen();
                    }
                }

                //Exit
                function exitFullscreen() {

                    if(document.exitFullscreen) {
                        document.exitFullscreen();
                    } else if(document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if(document.webkitExitFullscreen) {
                        document.webkitExitFullscreen();
                    }
                }

                launchIntoFullscreen(document.documentElement);

                break;


            /*-------------------------------------------
                Clear Local Storage
            ---------------------------------------------*/
            case 'clear-localstorage':
                swal({
                    title: "Are you sure?",
                    text: "All your saved localStorage values will be removed",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function(){
                    localStorage.clear();
                    swal("Done!", "localStorage is cleared", "success");
                });

                break;


            /*-------------------------------------------
                Print
            ---------------------------------------------*/
            case 'print':

                window.print();

                break;


            /*-------------------------------------------
                Login Window Switch
            ---------------------------------------------*/
            case 'login-switch':
                var loginblock = jQuerythis.data('ma-block');
                var loginParent = jQuerythis.closest('.lc-block');

                loginParent.removeClass('toggled');

                setTimeout(function(){
                    jQuery(loginblock).addClass('toggled');
                });

                break;


            /*-------------------------------------------
                Profile Edit/Edit Cancel
            ---------------------------------------------*/
            //Edit
            case 'profile-edit':
                jQuerythis.closest('.pmb-block').toggleClass('toggled');

                break;

            case 'profile-edit-cancel':
                jQuery(this).closest('.pmb-block').removeClass('toggled');

                break;


            /*-------------------------------------------
                Action Header Open/Close
            ---------------------------------------------*/
            //Open
            case 'action-header-open':
                ahParent = jQuerythis.closest('.action-header').find('.ah-search');

                ahParent.fadeIn(300);
                ahParent.find('.ahs-input').focus();

                break;

            //Close
            case 'action-header-close':
                ahParent.fadeOut(300);
                setTimeout(function(){
                    ahParent.find('.ahs-input').val('');
                }, 350);

                break;


            /*-------------------------------------------
                Wall Comment Open/Close
            ---------------------------------------------*/
            //Open
            case 'wall-comment-open':
                if(!(jQuerythis).closest('.wic-form').hasClass('toggled')) {
                    jQuerythis.closest('.wic-form').addClass('toggled');
                }

                break;

            //Close
            case 'wall-comment-close':
                jQuerythis.closest('.wic-form').find('textarea').val('');
                jQuerythis.closest('.wic-form').removeClass('toggled');

                break;


            /*-------------------------------------------
                Todo Form Open/Close
            ---------------------------------------------*/
            //Open
            case 'todo-form-open':
                jQuerythis.closest('.t-add').addClass('toggled');

                break;

            //Close
            case 'todo-form-close':
                jQuerythis.closest('.t-add').removeClass('toggled');
                jQuerythis.closest('.t-add').find('textarea').val('');

                break;
        }
    });
});

jQuery(document).ready(function () {
    /*-------------------------------------------
        Sparkline
    ---------------------------------------------*/
    function sparklineBar(id, values, height, barWidth, barColor, barSpacing) {
        jQuery('.'+id).sparkline(values, {
            type: 'bar',
            height: height,
            barWidth: barWidth,
            barColor: barColor,
            barSpacing: barSpacing
        })
    }

    function sparklineLine(id, values, width, height, lineColor, fillColor, lineWidth, maxSpotColor, minSpotColor, spotColor, spotRadius, hSpotColor, hLineColor) {
        jQuery('.'+id).sparkline(values, {
            type: 'line',
            width: width,
            height: height,
            lineColor: lineColor,
            fillColor: fillColor,
            lineWidth: lineWidth,
            maxSpotColor: maxSpotColor,
            minSpotColor: minSpotColor,
            spotColor: spotColor,
            spotRadius: spotRadius,
            highlightSpotColor: hSpotColor,
            highlightLineColor: hLineColor
        });
    }

    function sparklinePie(id, values, width, height, sliceColors) {
        jQuery('.'+id).sparkline(values, {
            type: 'pie',
            width: width,
            height: height,
            sliceColors: sliceColors,
            offset: 0,
            borderWidth: 0
        });
    }

    // Mini Chart - Bar Chart 1
    if (jQuery('.stats-bar')[0]) {
        sparklineBar('stats-bar', [6,4,8,6,5,6,7,8,3,5,9,5,8,4], '35px', 3, '#d6d8d9', 2);
    }

    // Mini Chart - Bar Chart 2
    if (jQuery('.stats-bar-2')[0]) {
        sparklineBar('stats-bar-2', [4,7,6,2,5,3,8,6,6,4,8,6,5,8], '35px', 3, '#d6d8d9', 2);
    }

    // Mini Chart - Line Chart 1
    if (jQuery('.stats-line')[0]) {
        sparklineLine('stats-line', [9,4,6,5,6,4,5,7,9,3,6,5], 68, 35, '#fff', 'rgba(0,0,0,0)', 1.25, '#d6d8d9', '#d6d8d9', '#d6d8d9', 3, '#fff', '#d6d8d9');
    }

    // Mini Chart - Line Chart 2
    if (jQuery('.stats-line-2')[0]) {
        sparklineLine('stats-line-2', [5,6,3,9,7,5,4,6,5,6,4,9], 68, 35, '#fff', 'rgba(0,0,0,0)', 1.25, '#d6d8d9', '#d6d8d9', '#d6d8d9', 3, '#fff', '#d6d8d9');
    }

    // Mini Chart - Pie Chart 1
    if (jQuery('.stats-pie')[0]) {
        sparklinePie('stats-pie', [20, 35, 30, 5], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
    }

    // Dash Widget Line Chart
    if (jQuery('.dash-widget-visits')[0]) {
        sparklineLine('dash-widget-visits', [9,4,6,5,6,4,5,7,9,3,6,5], '100%', '70px', 'rgba(255,255,255,0.7)', 'rgba(0,0,0,0)', 1, 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.4)', 5, 'rgba(255,255,255,0.4)', '#fff');
    }


    /*-------------------------------------------
        Easy Pie Charts
    ---------------------------------------------*/
    function easyPieChart(id, trackColor, scaleColor, barColor, lineWidth, lineCap, size) {
        jQuery('.'+id).easyPieChart({
            trackColor: trackColor,
            scaleColor: scaleColor,
            barColor: barColor,
            lineWidth: lineWidth,
            lineCap: lineCap,
            size: size
        });
    }

    // Main Pie Chart
    if (jQuery('.main-pie')[0]) {
        easyPieChart('main-pie', 'rgba(0,0,0,0.2)', 'rgba(255,255,255,0)', 'rgba(255,255,255,0.7)', 2, 'butt', 148);
    }

    // Others
    if (jQuery('.sub-pie-1')[0]) {
        easyPieChart('sub-pie-1', 'rgba(0,0,0,0.2)', 'rgba(255,255,255,0)', 'rgba(255,255,255,0.7)', 2, 'butt', 95);
    }

    if (jQuery('.sub-pie-2')[0]) {
        easyPieChart('sub-pie-2', 'rgba(0,0,0,0.2)', 'rgba(255,255,255,0)', 'rgba(255,255,255,0.7)', 2, 'butt', 95);
    }
});

// jQuery(window).load(function(){

//     /*-------------------------------------------
//         Welcome Message
//     ---------------------------------------------*/
//     function notify(message, type){
//         jQuery.growl({
//             message: message
//         },{
//             type: type,
//             allow_dismiss: false,
//             label: 'Cancel',
//             className: 'btn-xs btn-inverse',
//             placement: {
//                 from: 'bottom',
//                 align: 'left'
//             },
//             delay: 2500,
//             animate: {
//                     enter: 'animated fadeInUp',
//                     exit: 'animated fadeOutDown'
//             },
//             offset: {
//                 x: 30,
//                 y: 30
//             }
//         });
//     };
    
//     setTimeout(function () {
//         if (!jQuery('.login-content')[0]) {
//             notify('Welcome to C-Trade Online', 'default');
//         }
//     }, 1000)
// });
jQuery(document).ready(function(){
    

    /*----------------------------------------------
        Make some random data for Flot Line Chart
     ----------------------------------------------*/
    var data1 = [[1,60], [2,30], [3,50], [4,100], [5,10], [6,90], [7,85]];
    var data2 = [[1,20], [2,90], [3,60], [4,40], [5,100], [6,25], [7,65]];
    var data3 = [[1,100], [2,20], [3,60], [4,90], [5,80], [6,10], [7,5]];
    
    // Create an Array push the data + Draw the bars

    var barData = [
        {
            label: 'Tokyo',
            data: data1,
            color: '#dbdddd'
        },
        {
            label: 'Seoul',
            data: data2,
            color: '#636c72'
        },
        {
            label: 'Beijing',
            data: data3,
            color: '#3e4a51'
        }
    ]
    

    /*---------------------------------
        Let's create the chart
     ---------------------------------*/
    if (jQuery('#bar-chart')[0]) {
        jQuery.plot(jQuery("#bar-chart"), barData, {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.05,
                    order: 1,
                    fill: 1
                }
            },
            grid : {
                    borderWidth: 1,
                    borderColor: '#333c42',
                    show : true,
                    hoverable : true,
                    clickable : true
            },

            yaxis: {
                tickColor: '#333c42',
                tickDecimals: 0,
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#9f9f9f",
                },
                shadowSize: 0
            },

            xaxis: {
                tickColor: '#333c42',
                tickDecimals: 0,
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#9f9f9f"
                },
                shadowSize: 0
            },

            legend:{
                container: '.flc-bar',
                backgroundOpacity: 0.5,
                noColumns: 0,
                backgroundColor: "white",
                lineWidth: 0
            }
        });
    }
    

    /*---------------------------------
        Tooltips for Flot Charts
     ---------------------------------*/
    if (jQuery(".flot-chart")[0]) {
        jQuery(".flot-chart").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);
                jQuery(".flot-tooltip").html(item.series.label + " of " + x + " = " + y).css({top: item.pageY+5, left: item.pageX+5}).show();
            }
            else {
                jQuery(".flot-tooltip").hide();
            }
        });
        
        jQuery("<div class='flot-tooltip' class='chart-tooltip'></div>").appendTo("body");
    }
});
jQuery(document).ready(function(){

    /*-----------------------------------------
        Make some random data for the Chart
     -----------------------------------------*/
    var d1 = [];
    for (var i = 0; i <= 10; i += 1) {
        d1.push([i, parseInt(Math.random() * 30)]);
    }
    var d2 = [];
    for (var i = 0; i <= 20; i += 1) {
        d2.push([i, parseInt(Math.random() * 30)]);
    }
    var d3 = [];
    for (var i = 0; i <= 10; i += 1) {
        d3.push([i, parseInt(Math.random() * 30)]);
    }


    /*---------------------------------
        Chart Options
     ---------------------------------*/
    var options = {
        series: {
            shadowSize: 0,
            curvedLines: { //This is a third party plugin to make curved lines
                apply: true,
                active: true,
                monotonicFit: true
            },
            lines: {
                show: false,
                lineWidth: 0,
            },
        },
        grid: {
            borderWidth: 0,
            labelMargin:10,
            hoverable: true,
            clickable: true,
            mouseActiveRadius:6,

        },
        xaxis: {
            tickDecimals: 0,
            ticks: false
        },

        yaxis: {
            tickDecimals: 0,
            ticks: false
        },

        legend: {
            show: false
        }
    };


    /*---------------------------------
        Let's create the chart
     ---------------------------------*/
    if (jQuery("#curved-line-chart")[0]) {
        jQuery.plot(jQuery("#curved-line-chart"), [
            {data: d1, lines: { show: true}, label: 'Product 1', stack: true, color: '#1f292f' },
            {data: d3, lines: { show: true}, label: 'Product 2', stack: true, color: '#dbdddd' }
        ], options);
    }


    /*---------------------------------
        Tooltips for Flot Charts
     ---------------------------------*/
    if (jQuery(".flot-chart")[0]) {
        jQuery(".flot-chart").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);
                jQuery(".flot-tooltip").html(item.series.label + " of " + x + " = " + y).css({top: item.pageY+5, left: item.pageX+5}).show();
            }
            else {
                jQuery(".flot-tooltip").hide();
            }
        });

        jQuery("<div class='flot-tooltip' class='chart-tooltip'></div>").appendTo("body");
    }
});

jQuery(document).ready(function(){

    /*---------------------------------
        Make some random data
     ---------------------------------*/
    var data = [];
    var totalPoints = 300;
    var updateInterval = 30;
    
    function getRandomData() {
        if (data.length > 0)
            data = data.slice(1);

        while (data.length < totalPoints) {
    
            var prev = data.length > 0 ? data[data.length - 1] : 50,
                y = prev + Math.random() * 10 - 5;
            if (y < 0) {
                y = 0;
            } else if (y > 90) {
                y = 90;
            }

            data.push(y);
        }

        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]])
        }

        return res;
    }


    /*---------------------------------
        Create Chart
     ---------------------------------*/
    if (jQuery('#dynamic-chart')[0]) {
        var plot = jQuery.plot("#dynamic-chart", [ getRandomData() ], {
            series: {
                label: "Server Process Data",
                lines: {
                    show: true,
                    lineWidth: 0.2,
                    fill: 0.6
                },
    
                color: '#fff',
                shadowSize: 0,
            },
            yaxis: {
                min: 0,
                max: 100,
                tickColor: '#333c42',
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#9f9f9f",
                },
                shadowSize: 0,
    
            },
            xaxis: {
                tickColor: '#333c42',
                show: true,
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#9f9f9f",
                },
                shadowSize: 0,
                min: 0,
                max: 250
            },
            grid: {
                borderWidth: 1,
                borderColor: '#333c42',
                labelMargin:10,
                hoverable: true,
                clickable: true,
                mouseActiveRadius:6,
            },
            legend:{
                container: '.flc-dynamic',
                backgroundOpacity: 0.5,
                noColumns: 0,
                backgroundColor: "white",
                lineWidth: 0
            }
        });



        /*---------------------------------
            Update
         ---------------------------------*/
        function update() {
            plot.setData([getRandomData()]);
            // Since the axes don't change, we don't need to call plot.setupGrid()

            plot.draw();
            setTimeout(update, updateInterval);
        }
        update();
    }
});
jQuery(document).ready(function(){

    /*---------------------------------------------------
        Make some random data for Recent Items chart
    ---------------------------------------------------*/
    var data = [];
    var totalPoints = 100;

    function getRandomData() {
        if (data.length > 0)
            data = data.slice(1);

        while (data.length < totalPoints) {

            var prev = data.length > 0 ? data[data.length - 1] : 50,
                y = prev + Math.random() * 10 - 5;
            if (y < 0) {
                y = 0;
            } else if (y > 90) {
                y = 90;
            }

            data.push(y);
        }

        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]])
        }

        return res;
    }

    /*---------------------------------------------------
        Make some random data for Flot Line Chart
    ---------------------------------------------------*/
    var d1 = [];
    for (var i = 0; i <= 10; i += 1) {
        d1.push([i, parseInt(Math.random() * 30)]);
    }
    var d2 = [];
    for (var i = 0; i <= 20; i += 1) {
        d2.push([i, parseInt(Math.random() * 30)]);
    }
    var d3 = [];
    for (var i = 0; i <= 10; i += 1) {
        d3.push([i, parseInt(Math.random() * 30)]);
    }

    /*---------------------------------
        Chart Options
    ---------------------------------*/
    var options = {
        series: {
            shadowSize: 0,
            lines: {
                show: false,
                lineWidth: 0,
            },
        },
        grid: {
            borderWidth: 0,
            labelMargin:10,
            hoverable: true,
            clickable: true,
            mouseActiveRadius:6,

        },
        xaxis: {
            tickDecimals: 0,
            ticks: false
        },

        yaxis: {
            tickDecimals: 0,
            ticks: false
        },

        legend: {
            show: false
        }
    };

    /*---------------------------------
        Chart Options
    ---------------------------------*/
    var options2 = {
        series: {
            shadowSize: 0,
            lines: {
                show: false,
                lineWidth: 0,
            },
        },
        grid: {
            borderWidth: 0,
            labelMargin:10,
            hoverable: true,
            clickable: true,
            mouseActiveRadius:6,

        },
        xaxis: {
            tickDecimals: 0,
            ticks: false
        },

        yaxis: {
            tickDecimals: 0,
            ticks: false
        },

        legend: {
            show: false
        }
    };


    /*---------------------------------
        Regular Line Chart
    ---------------------------------*/
    if (jQuery("#line-chart")[0]) {
        jQuery.plot(jQuery("#line-chart"), [
            {data: d1, lines: { show: true, fill: 1.01 }, label: 'OMZIL Stock Price', stack: true, color: '#ffffff', points: { symbol: "triangle"}  },
        ], options2);
    }


    /*---------------------------------
        Recent Items Table Chart
    ---------------------------------*/
    if (jQuery("#recent-items-chart")[0]) {
        jQuery.plot(jQuery("#recent-items-chart"), [
            {data: getRandomData(), lines: { show: true, fill: 0.1 }, label: 'Items', stack: true, color: '#dbdddd' },
        ], options);
    }


    /*---------------------------------
        Tooltips for Flot Charts
    ---------------------------------*/
    if (jQuery(".flot-chart")[0]) {
        jQuery(".flot-chart").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);
                jQuery(".flot-tooltip").html(item.series.label + " of " + x + " = " + y).css({top: item.pageY+5, left: item.pageX+5}).show();
            }
            else {
                jQuery(".flot-tooltip").hide();
            }
        });

        jQuery("<div class='flot-tooltip' class='chart-tooltip'></div>").appendTo("body");
    }
});

jQuery(document).ready(function(){

    var data1 = [[2010,60], [2011,50], [2012,80], [2013,30], [2014,70], [2015,40], [2016,55]];

    var dataset = [
        {
            label: "Index Value",
            data: data1,
            color: "#00BCD4",
            points: {
                fillColor: "#00BCD4",
                show: true,
                radius: 0
            },
            lines: {
                show: true,
                lineWidth: 1,
                fill: 1,
                fillColor: {
                    colors: ["rgba(255,255,255,0.0)","#00BCD4"]
                }
            }
        }
    ];

    var options = {
        series: {
            shadowSize: 0,
            lines: {
                show: false,
                lineWidth: 0,
            },
        },

        grid : {
            borderWidth: 1,
            borderColor: '#eee',
            show : false,
            hoverable : true,
            clickable : true
        },

        yaxis: {
            tickColor: '#eee',
            tickDecimals: 0,
            font :{
                lineHeight: 13,
                style: "normal",
                color: "#9f9f9f",
            },
            shadowSize: 0
        },

        xaxis: {
            tickColor: '#fff',
            tickDecimals: 0,
            font :{
                lineHeight: 13,
                style: "normal",
                color: "#9f9f9f"
            },
            shadowSize: 0,
        },

        legend:{
            container: '.flc-sei',
            backgroundOpacity: 0.5,
            noColumns: 0,
            backgroundColor: "white",
            lineWidth: 0
        }
    }

    if (jQuery('#effective-index')[0]) {
        jQuery.plot(jQuery("#effective-index"), dataset, options);
    }
});


jQuery(document).ready(function(){
    var pieData = [
        {data: 1, color: '#dbdddd', label: 'Toyota'},
        {data: 2, color: '#636c72', label: 'Nissan'},
        {data: 3, color: '#3e4a51', label: 'Hyundai'},
        {data: 4, color: '#1f292f', label: 'Scion'},
        {data: 4, color: '#ffffff', label: 'Daihatsu'},
    ]


    /*---------------------------------
        Pie Chart
    ---------------------------------*/
    if(jQuery('#pie-chart')[0]){
        jQuery.plot('#pie-chart', pieData, {
            series: {
                pie: {
                    show: true,
                    stroke: {
                        width: 0,
                    },
                },
            },
            legend: {
                container: '.flc-pie',
                backgroundOpacity: 0.5,
                noColumns: 0,
                backgroundColor: "white",
                lineWidth: 0
            },
            grid: {
                hoverable: true,
                clickable: true
            },
            tooltip: true,
            tooltipOpts: {
                content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                shifts: {
                    x: 20,
                    y: 0
                },
                defaultTheme: false,
                cssClass: 'flot-tooltip'
            }

        });
    }


    /*---------------------------------
        Donut Chart
    ---------------------------------*/
    if(jQuery('#donut-chart')[0]){
        jQuery.plot('#donut-chart', pieData, {
            series: {
                pie: {
                    innerRadius: 0.5,
                    show: true,
                    stroke: {
                        width: 0,
                        color: '#2b343a'
                    },
                },
            },
            legend: {
                container: '.flc-donut',
                backgroundOpacity: 0.5,
                noColumns: 0,
                backgroundColor: "white",
                lineWidth: 0
            },
            grid: {
                hoverable: true,
                clickable: true
            },
            tooltip: true,
            tooltipOpts: {
                content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                shifts: {
                    x: 20,
                    y: 0
                },
                defaultTheme: false,
                cssClass: 'flot-tooltip'
            }
        });
    }
});

/*----------------------------------------------------------
    Detect Mobile Browser
-----------------------------------------------------------*/
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
   jQuery('html').addClass('ismobile');
}

jQuery(window).load(function () {
    /*-----------------------------------------------------------
        Page Loader
     -----------------------------------------------------------*/
    if(jQuery('.page-loader')[0]) {
        setTimeout (function () {
            jQuery('.page-loader').fadeOut();
        }, 500);

    }
})

jQuery(document).ready(function(){

    /*----------------------------------------------------------
        Scrollbar
    -----------------------------------------------------------*/
    function scrollBar(selector, theme, mousewheelaxis) {
        jQuery(selector).mCustomScrollbar({
            theme: theme,
            scrollInertia: 100,
            axis:'mousewheelaxis',
            mouseWheel: {
                enable: true,
                axis: mousewheelaxis,
                preventDefault: true
            }
        });
    }

    if (!jQuery('html').hasClass('ismobile')) {
        //On Custom Class
        if (jQuery('.c-overflow')[0]) {
            scrollBar('.c-overflow', 'minimal-dark', 'y');
        }
    }

    /*----------------------------------------------------------
        Dropdown Menu
    -----------------------------------------------------------*/
    if(jQuery('.dropdown')[0]) {
  	   //Propagate
    	jQuery('body').on('click', '.dropdown.open .dropdown-menu', function(e){
    	    e.stopPropagation();
    	});

    	jQuery('.dropdown').on('shown.bs.dropdown', function (e) {
    	    if(jQuery(this).attr('data-animation')) {
        		jQueryanimArray = [];
        		jQueryanimation = jQuery(this).data('animation');
        		jQueryanimArray = jQueryanimation.split(',');
        		jQueryanimationIn = 'animated '+jQueryanimArray[0];
        		jQueryanimationOut = 'animated '+ jQueryanimArray[1];
        		jQueryanimationDuration = ''
        		if(!jQueryanimArray[2]) {
        		    jQueryanimationDuration = 500; //if duration is not defined, default is set to 500ms
        		}
        		else {
        		    jQueryanimationDuration = jQueryanimArray[2];
        		}

        		jQuery(this).find('.dropdown-menu').removeClass(jQueryanimationOut)
        		jQuery(this).find('.dropdown-menu').addClass(jQueryanimationIn);
    	    }
    	});

	     jQuery('.dropdown').on('hide.bs.dropdown', function (e) {
    	    if(jQuery(this).attr('data-animation')) {
        		e.preventDefault();
        		jQuerythis = jQuery(this);
        		jQuerydropdownMenu = jQuerythis.find('.dropdown-menu');

        		jQuerydropdownMenu.addClass(jQueryanimationOut);
        		setTimeout(function(){
        		    jQuerythis.removeClass('open')

        		}, jQueryanimationDuration);
        	}
        });
      }


    /*----------------------------------------------------------
        Calendar Widget
    -----------------------------------------------------------*/
    if(jQuery('#calendar-widget')[0]) {
        (function(){
            jQuery('#calendar-widget #cw-body').fullCalendar({
		        contentHeight: 'auto',
		        theme: true,
                header: {
                    right: '',
                    center: 'prev, title, next',
                    left: ''
                },
                defaultDate: '2014-06-12',
                editable: true,
                events: [
                    {
                        title: 'All Day',
                        start: '2014-06-01',
                    },
                    {
                        title: 'Long Event',
                        start: '2014-06-07',
                        end: '2014-06-10',
                    },
                    {
                        id: 999,
                        title: 'Repeat',
                        start: '2014-06-09',
                    },
                    {
                        id: 999,
                        title: 'Repeat',
                        start: '2014-06-16',
                    },
                    {
                        title: 'Meet',
                        start: '2014-06-12',
                        end: '2014-06-12',
                    },
                    {
                        title: 'Lunch',
                        start: '2014-06-12',
                    },
                    {
                        title: 'Birthday',
                        start: '2014-06-13',
                    },
                    {
                        title: 'Google',
                        url: 'http://google.com/',
                        start: '2014-06-28',
                    }
                ]
            });

            //Display Current Date as Calendar widget header
            var mYear = moment().format('YYYY');
            var mDay = moment().format('dddd, MMM D');
            jQuery('#calendar-widget .cwh-year').html(mYear);
            jQuery('#calendar-widget .cwh-day').html(mDay);
        })();
    }


    /*----------------------------------------------------------
        Weather Widget
    -----------------------------------------------------------*/
    if (jQuery('#weather-widget')[0]) {
        jQuery.simpleWeather({
            location: 'Austin, TX',
            woeid: '',
            unit: 'f',
            success: function(weather) {
                html = '<div class="weather-status">'+weather.temp+'&deg;'+weather.units.temp+'</div>';
                html += '<ul class="weather-info"><li>'+weather.city+', '+weather.region+'</li>';
                html += '<li class="currently">'+weather.currently+'</li></ul>';
                html += '<div class="weather-icon wi-'+weather.code+'"></div>';
                html += '<div class="dw-footer"><div class="weather-list tomorrow">';
                html += '<span class="weather-list-icon wi-'+weather.forecast[2].code+'"></span><span>'+weather.forecast[1].high+'/'+weather.forecast[1].low+'</span><span>'+weather.forecast[1].text+'</span>';
                html += '</div>';
                html += '<div class="weather-list after-tomorrow">';
                html += '<span class="weather-list-icon wi-'+weather.forecast[2].code+'"></span><span>'+weather.forecast[2].high+'/'+weather.forecast[2].low+'</span><span>'+weather.forecast[2].text+'</span>';
                html += '</div></div>';
                jQuery("#weather-widget").html(html);
            },
            error: function(error) {
                jQuery("#weather-widget").html('<p>'+error+'</p>');
            }
        });
    }


    /*----------------------------------------------------------
         Auto Size Textare
    -----------------------------------------------------------*/
    if (jQuery('.auto-size')[0]) {
	     autosize(jQuery('.auto-size'));
    }


    /*----------------------------------------------------------
        Text Field
    -----------------------------------------------------------*/
    //Add blue animated border and remove with condition when focus and blur
    if(jQuery('.fg-line')[0]) {
        jQuery('body').on('focus', '.fg-line .form-control', function(){
            jQuery(this).closest('.fg-line').addClass('fg-toggled');
        })

        jQuery('body').on('blur', '.form-control', function(){
            var p = jQuery(this).closest('.form-group, .input-group');
            var i = p.find('.form-control').val();

            if (p.hasClass('fg-float')) {
                if (i.length == 0) {
                    jQuery(this).closest('.fg-line').removeClass('fg-toggled');
                }
            }
            else {
                jQuery(this).closest('.fg-line').removeClass('fg-toggled');
            }
        });
    }

    //Add blue border for pre-valued fg-flot text feilds
    if(jQuery('.fg-float')[0]) {
        jQuery('.fg-float .form-control').each(function(){
            var i = jQuery(this).val();

            if (!i.length == 0) {
                jQuery(this).closest('.fg-line').addClass('fg-toggled');
            }

        });
    }


    /*----------------------------------------------------------
        Audio and Video Player
    -----------------------------------------------------------*/
    if(jQuery('audio, video')[0]) {
        jQuery('video,audio').mediaelementplayer();
    }


    /*----------------------------------------------------------
        Chosen
    -----------------------------------------------------------*/
    if(jQuery('.chosen')[0]) {
        jQuery('.chosen').chosen({
            width: '100%',
            allow_single_deselect: true
        });
    }


    /*----------------------------------------------------------
        NoUiSlider (Input Slider)
    -----------------------------------------------------------*/
    //Basic
    if (jQuery('#input-slider')[0]) {
        var slider = document.getElementById ('input-slider');

        noUiSlider.create (slider, {
            start: [20],
            connect: 'lower',
            range: {
                'min': 0,
                'max': 100
            }
        });
    }

    //Range
    if (jQuery('#input-slider-range')[0]) {
        var sliderRange = document.getElementById ('input-slider-range');

        noUiSlider.create (sliderRange, {
            start: [40, 70],
            connect: true,
            range: {
                'min': 0,
                'max': 100
            }
        });
    }

    //Range with value
    if(jQuery('#input-slider-value')[0]) {
        var sliderRangeValue = document.getElementById('input-slider-value');

        noUiSlider.create(sliderRangeValue, {
            start: [10, 50],
            connect: true,
            range: {
                'min': 0,
                'max': 100
            }
        });

        sliderRangeValue.noUiSlider.on('update', function( values, handle ) {
            document.getElementById('input-slider-value-output').innerHTML = values[handle];
        });
    }


    /*----------------------------------------------------------
        Input Mask
    -----------------------------------------------------------*/
    if (jQuery('input-mask')[0]) {
        jQuery('.input-mask').mask();
    }


    /*----------------------------------------------------------
        Farbtastic Color Picker
    -----------------------------------------------------------*/
    if (jQuery('.color-picker')[0]) {
	    jQuery('.color-picker').each(function(){
            var colorOutput = jQuery(this).closest('.cp-container').find('.cp-value');
            jQuery(this).farbtastic(colorOutput);
        });
    }


    /*-----------------------------------------------------------
        Summernote HTML Editor
    -----------------------------------------------------------*/
    if (jQuery('.html-editor')[0]) {
	   jQuery('.html-editor').summernote({
            height: 150
        });
    }

    if(jQuery('.html-editor-click')[0]) {
        //Edit
        jQuery('body').on('click', '.hec-button', function(){
            jQuery('.html-editor-click').summernote({
                focus: true
            });
            jQuery('.hec-save').show();
        })

        //Save
        jQuery('body').on('click', '.hec-save', function(){
            jQuery('.html-editor-click').code();
            jQuery('.html-editor-click').destroy();
            jQuery('.hec-save').hide();
        });
    }

    //Air Mode
    if(jQuery('.html-editor-airmod')[0]) {
        jQuery('.html-editor-airmod').summernote({
            airMode: true
        });
    }


    /*-----------------------------------------------------------
        Date Time Picker
    -----------------------------------------------------------*/
    //Date Time Picker
    if (jQuery('.date-time-picker')[0]) {
	   jQuery('.date-time-picker').datetimepicker();
    }

    //Time
    if (jQuery('.time-picker')[0]) {
    	jQuery('.time-picker').datetimepicker({
    	    format: 'LT'
    	});
    }

    //Date
    if (jQuery('.date-picker')[0]) {
    	jQuery('.date-picker').datetimepicker({
    	    format: 'DD/MM/YYYY'
    	});
    }

    jQuery('.date-picker').on('dp.hide', function(){
        jQuery(this).closest('.dtp-container').removeClass('fg-toggled');
        jQuery(this).blur();
    })


    /*-----------------------------------------------------------
        Form Wizard
    -----------------------------------------------------------*/
    if (jQuery('.form-wizard-basic')[0]) {
    	jQuery('.form-wizard-basic').bootstrapWizard({
    	    tabClass: 'fw-nav',
            'nextSelector': '.next',
            'previousSelector': '.previous'
    	});
    }


    /*-----------------------------------------------------------
        Waves
    -----------------------------------------------------------*/
    (function(){
         Waves.attach('.btn:not(.btn-icon):not(.btn-float)');
         Waves.attach('.btn-icon, .btn-float', ['waves-circle', 'waves-float']);
        Waves.init();
    })();


    /*----------------------------------------------------------
        Lightbox
    -----------------------------------------------------------*/
    if (jQuery('.lightbox')[0]) {
        jQuery('.lightbox').lightGallery({
            enableTouch: true
        });
    }


    /*-----------------------------------------------------------
        Link prevent
    -----------------------------------------------------------*/
    jQuery('body').on('click', '.a-prevent', function(e){
        e.preventDefault();
    });


    /*----------------------------------------------------------
        Bootstrap Accordion Fix
    -----------------------------------------------------------*/
    if (jQuery('.collapse')[0]) {

        //Add active class for opened items
        jQuery('.collapse').on('show.bs.collapse', function (e) {
            jQuery(this).closest('.panel').find('.panel-heading').addClass('active');
        });

        jQuery('.collapse').on('hide.bs.collapse', function (e) {
            jQuery(this).closest('.panel').find('.panel-heading').removeClass('active');
        });

        //Add active class for pre opened items
        jQuery('.collapse.in').each(function(){
            jQuery(this).closest('.panel').find('.panel-heading').addClass('active');
        });
    }


    /*-----------------------------------------------------------
        Tooltips
    -----------------------------------------------------------*/
    if (jQuery('[data-toggle="tooltip"]')[0]) {
        jQuery('[data-toggle="tooltip"]').tooltip();
    }


    /*-----------------------------------------------------------
        Popover
    -----------------------------------------------------------*/
    if (jQuery('[data-toggle="popover"]')[0]) {
        jQuery('[data-toggle="popover"]').popover();
    }


    /*-----------------------------------------------------------
        IE 9 Placeholder
    -----------------------------------------------------------*/
    if(jQuery('html').hasClass('ie9')) {
        jQuery('input, textarea').placeholder({
            customClass: 'ie9-placeholder'
        });
    }


    /*-----------------------------------------------------------
        Typeahead Auto Complete
    -----------------------------------------------------------*/
     if(jQuery('.typeahead')[0]) {

          var statesArray = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
            'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
            'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
            'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
            'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
            'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
            'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
            'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
            'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
          ];
        var states = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: statesArray
        });

        jQuery('.typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
          name: 'states',
          source: states
        });
    }

    /*-----------------------------------------------------------
        Dropzone Uploader
    -----------------------------------------------------------*/
    if(jQuery('.dropzone')[0]) {
        Dropzone.autoDiscover = false;
        jQuery('#dropzone-upload').dropzone({
            url: "/file/post",
            addRemoveLinks: true

        });
    }
});
