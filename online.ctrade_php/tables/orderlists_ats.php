<?php

require_once 'db_cds/DbConnectATS.php';
$db_cds = new DbConnectATS();
$conn_cds = $db_cds->connect();

$sqel = "SELECT * from 
(select OrderNo, OrderType,  Company, CDS_Ac_No as [CDS_Number], Broker_Code as [Broker], ClientName, case OrderStatus 
when 'OPEN' then 'Pending Authorization' 
when 'POSTED' then 'Authorized' end as [Order_Status], CONVERT(VARCHAR(24),Create_date,113) as [Order_Date], Quantity 
from Order_Live WHERE OrderStatus='OPEN'  
union 
select OrderNo, OrderType,  Company, CDS_Ac_No as [CDS_Number], Broker_Code as [Broker], ClientName, 
'Authorized' as [Order_Status], CONVERT(VARCHAR(24),OrderDate,113) as [Order_Date], Quantity 
from LiveTradingMaster  
union   
select OrderNo, OrderType,  Company, CDS_Ac_No as [CDS_Number], Broker_Code as [Broker], ClientName, case DealStatus 
when 'Completed' then 'Matched' end as [Order_Status], CONVERT(VARCHAR(24),OrderDate,113) as [Order_Date], Quantity 
from LiveTradingCompletedOrders) j WHERE  [CDS_Number] = '".$_SESSION['cds_no']."' " ; 

$result=sqlsrv_query($conn_cds,$sqel);

        $found = 0 ;
        if( $result === false ){
             // echo "Error in executing query.</br>";
             echo "<tr style='width: 100%;''><td align='center'colspan='9'>0 results</td></tr>";
             die( print_r( sqlsrv_errors(), true));
             return false;
        }else{
            while ($row = sqlsrv_fetch_array($result)){     
                
                echo "
                          <tr>
                                <td>".$row['OrderNo']."</td>
                                <td>".$row['OrderType']."</td>
                                <td>".$row['Company']."</td>                                                
                                <td>".$row['CDS_Number']."</td>                                                
                                <td>".$row['Broker']."</td>                                                
                                <td>".$row['Order_Status']."</td>
                                <td>".$row['Quantity']."</td>
                                <td>".$row['Order_Date']."</td>

                            </tr>" ; 

            }

        }
        sqlsrv_free_stmt( $result);
        sqlsrv_close( $conn_cds);


?>

