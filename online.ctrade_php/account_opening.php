<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<html lang="en" ng-app="myApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C-Trade Online</title>

        <!-- Vendor CSS -->
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
        <link href="vendors/bower_components/google-material-color/dist/palette.css" rel="stylesheet">

        <link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="vendors/bower_components/chosen/chosen.min.css" rel="stylesheet">
        <link href="vendors/summernote/dist/summernote.css" rel="stylesheet">

        <!-- CSS -->
        <link href="css/style.css" rel="stylesheet">
        <link href="css/app.min.1.css" rel="stylesheet">
        <link href="css/app.min.2.css" rel="stylesheet">
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="css/buttons.dataTables.min.css" rel="stylesheet">
        
        <!-- Following CSS are used only for the Demp purposes thus you can remove this anytime. -->
        <style type="text/css">
            .toggle-switch .ts-label {
                min-width: 130px;
            }
            .error{
                color: #F44336 ; 
            }
        </style>
            <!-- CSS -->
            <link href="css/bootstrap.css" rel="stylesheet">
            <link href="css/style.css" rel="stylesheet">
            <link href="font-awesome/css/font-awesome.css" rel="stylesheet" >
            <link href="css/socialize-bookmarks.css" rel="stylesheet">
            <link href="js/fancybox/source/jquery.fancybox.css?v=2.1.4" rel="stylesheet">
            <link href="check_radio/skins/square/aero.css" rel="stylesheet">
            <!-- Toggle Switch -->
            <link rel="stylesheet" href="css/jquery.switch.css">
            <!-- Owl Carousel Assets -->
            <link href="css/owl.carousel.css" rel="stylesheet">
            <link href="css/owl.theme.css" rel="stylesheet">
            <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
            <!-- Jquery -->
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/jquery-ui-1.8.22.min.js"></script>
            <!-- Wizard-->
            <script src="js/jquery.wizard.js"></script>
            <!-- Radio and checkbox styles -->
            <script src="check_radio/jquery.icheck.js"></script>
            <!-- HTML5 and CSS3-in older browsers-->
            <script src="js/modernizr.custom.17475.js"></script>
            <!-- Support media queries for IE8 -->
            <script src="js/respond.min.js"></script>     
    </head>

    <body  ng-cloak="">
        <header id="header" class="clearfix">
            <ul class="h-inner">
                <li class="hidden-xs">
                    <a href="index.html" class="p-l-10">
                        <img src="img/demo/logo.png" style="width: 34%;" alt="">
                    </a>
                </li>

                <li class="pull-right">
                    <ul class="hi-menu">


                        <li class="dropdown" >
                            <a data-toggle="dropdown" href=""><span class="him-label">Guest</span></a>
                            <ul class="dropdown-menu dm-icon pull-right">
                                <li class="hidden-xs">
                                    <a data-ma-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a>
                                </li>
                                <li>
                                    <a class ="cd-signin" href="javascript:void(0);" ng-controller="notLoggedCtrl" ng-click="openDiag()"> Login</a>
                                </li>
                                <li>
                                    <a class ="cd-signup"  href="http://localhost/ctrade/sign_up.php" target="_blank">Create Account</a>
                                </li>
                                <li>
                                    <a href="">Settings</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>

        </header>

        <section id="main">

            <section id="content">
                <div class="container" style="margin-top: 430px;">

                   <div id="survey_container">
                           <div id="top-wizard">
                              <strong>Progress <span id="location"></span></strong>
                              <div id="progressbar"></div>
                              <div class="shadow"></div>
                           </div>  

                        <div class="row">                        
                            <div class="col-sm-12 col-xs-6" >

                                        <!-- Form -->
                                        <div class="card" id ="place_order">
                                            
                                            <div class="card-header ch-alt">
                                                <h1 style="text-align:left;">
                                                    Create Account </h1>
                                                <h2>
                                                    <small>Create a cds account </small>
                                                </h2>
                                            </div>

                                            <div class="card-body card-padding">
                                                <form name  = "invoice" id="wrappeddddd" action = "" method = "POST">
                                                    <div class="row">
                                                      <div id="middle-wizard">

                                                         
                                                        <div class="step">                                                    

                                                            <input type ="hidden" name= "acc_type" value="i" >
                                                            <div class="col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-sm-2  ">
                                                                        <label class="c-blue" >Title</label>
                                                                        <select name  = "my_title" class="selectpicker required" >
                                                                            <option value = "Mr">Mr</option>
                                                                            <option value = "Mrs">Mrs</option>
                                                                        </select>
                                                                    </div>  


                                                                    <div class="col-sm-5 m-b-20">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Forenames <br></label>
                                                                            <input type="text" name  = "fore_name" id = "fore_name" class="form-control required"  placeholder="Forenames">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-5 m-b-20">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Surname <br></label>
                                                                            <input type="text" name  = "surname" id = "surname" class="form-control required"  placeholder="Surname">
                                                                        </div>
                                                                    </div>    
                                                                </div>
                                                            </div> 

                                                            <div class="col-sm-12">
                                                                <div class="row">

                                                                    <div class="col-sm-4  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Date of Birth <br></label>
                                                                            <input type="text" name="dob" id="dob" class="form-control required date-picker valid" placeholder="Click here..." aria-required="true" aria-invalid="false">
                                                                            <!-- <input type="text" name  = "" id = "my_price" class="form-control "  placeholder="Date of Birth"> -->
                                                                        </div>
                                                                    </div> 
                                                                    <div class="col-sm-4  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Gender </label><br>
                                                                            <input type="radio" class ="required" id ="genr" name="gender" value="Male"  >
                                                                            <label for="compete_no">Male</label>

                                                                            <input type="radio" class ="required" id ="genr" name="gender" value="Female">
                                                                            <label for="compete_yes">Female</label>
                                                                        </div>
                                                                    </div>  

                                                                    <div class="col-sm-4  ">

                                                                    </div>    
                                                            
                                                                </div>    
                                                            </div> 
          

                                                            <div class="col-sm-12">
                                                                <div class="row">

                                                                    <div class="col-sm-6  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">National ID<br></label>
                                                                            <input type="text" name  = "nat_id" id = "nat_id" class="form-control required"  placeholder="National ID">
                                                                        </div>
                                                                    </div> 
                                                                    <div class="col-sm-6  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Password<br></label>
                                                                            <input type="password" name  = "my_pass" id = "pass" class="form-control required"  placeholder="Password">
                                                                        </div>
                                                                    </div> 
           
                                                                </div>     
                                                            </div>   

                                                            <div class="col-sm-12">
                                                                <!-- /Row -->   
                                                                <div class="row">
                                                                   <div class="col-md-4 col-md-offset-4">
                                                                      <ul class="data-list" id="terms">
                                                                         <li>
                                                                            <strong>Do you accept <a href="#" data-toggle="modal" data-target="#terms-txt">terms and conditions</a> ?</strong>
                                                                            <label class="switch-light switch-ios ">
                                                                            <input type="checkbox" name="terms" class="form-control  required fix_ie8" value="yes">
                                                                            <span>
                                                                            <span class="ie8_hide">No</span>
                                                                            <span>Yes</span>
                                                                            </span>
                                                                            <a></a>
                                                                            </label>
                                                                         </li>
                                                                      </ul>
                                                                   </div>
                                                                </div>                                                            
                                                            </div>
                                                        </div>

                                                        <div class="step"> 
                                                            <div class="col-sm-12">
                                                                Location <br>
                                                                <div class="row">

                                                                    <div class="col-sm-6  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Address-Home<br></label>
                                                                            <input type="text" name  = "add_home" id = "add" class="form-control required"  placeholder="Address-Home">
                                                                        </div>
                                                                    </div> 

                                                                    <div class="col-sm-6  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Address-Business <br></label>
                                                                            <input type="text" name  = "add_bus" id = "addr" class="form-control required"  placeholder="Address-Business1">
                                                                        </div>
                                                                    </div>     
                                                                </div>     
                                                            </div>  


                                                            <div class="col-sm-12">
                                                                <div class="row">

                                                                    <div class="col-sm-4  ">

                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Country <br></label>
                                                                            <select name  = "country" class="selectpicker required" id="cont" >
                                                                               <?php include("cont.php") ; ?>
                                                                            </select>                                                                            
                                                                            <!-- <input type="text" name  = "" id = "my_price1" class="form-control "  placeholder="Country"> -->
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-4  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">City<br></label>
                                                                            <input type="text" name  = "city" id = "city" class="form-control required"  placeholder="City">
                                                                        </div>
                                                                    </div> 

                                                                    <div class="col-sm-4  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Mobile Number <br></label>
                                                                            <input type="text" name  = "cell_num" id = "mobile" class="form-control required"  placeholder="Mobile Number ">
                                                                        </div>
                                                                    </div>

                                                                </div>     
                                                            </div>     

                                                            <div class="col-sm-12">
                                                                <div class="row">

                                                                    <div class="col-sm-4  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Telephone<br></label>
                                                                            <input type="text" name  = "telphone" id = "tele" class="form-control required"  placeholder="Telephone">
                                                                        </div>
                                                                    </div> 

                                                                    <div class="col-sm-8  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Email <br></label>
                                                                            <input type="email" name  = "my_email" id = "email" class="form-control required"  placeholder="Email">
                                                                        </div>
                                                                    </div>     
                                                                </div>     
                                                            </div>  
                                                        </div>

                                                        
                                                        <div class="step">  

                                                            <div class="col-sm-12">
                                                                Banking Details <br>
                                                                <div class="row">

                                                                    <div class="col-sm-4  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Bank Name<br></label>
                                                                            <select name  = "bank_name" class="selectpicker required" id ="bank_name">
                                                                               <?php include("banks.php") ; ?>
                                                                            </select>     

                                                                            <!-- <input type="text" name  = "" id = "my_price1" class="form-control "  placeholder="Bank Name"> -->
                                                                        </div>
                                                                    </div> 

                                                                    <div class="col-sm-4  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Branch <br></label>

                                                                            <select name  = "branch" class="selectpicker required" id ="branch">
                                                                              <option value = "0">  Please Select bank first  </option>
                                                                            </select>                                                                                 
                                                                            <!-- <input type="text" name  = "" id = "" class="form-control required"  placeholder="Branch"> -->
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-4  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Account Number <br></label>
                                                                            <input type="text" name  = "acc_num" id = "acc_num" class="form-control required"  placeholder="Account Number">
                                                                        </div>
                                                                    </div>

                                                                </div>     
                                                            </div>     

                                                            <div class="col-sm-12">
                                                                <div class="row">

                                                                    <div class="col-sm-4  ">
                                                                        <div class="form-group fg-line">
                                                                            <label  class="c-blue">Stockbrocking Firm<br></label>
                                                                                <select name  = "stock_brocker" class="selectpicker required" data-live-search="true" id ="stockbrocker">
                                                                                    <option value = "ABCS">   ABC STOCKBROKERS  </option>
                                                                                    <option value = "AKRI">   AKRIBOS SECURITIES (PVT) LTD  </option>
                                                                                    <option value = "AutoInvest">   Auto Investments  </option>
                                                                                    <option value = "BEAR">   BEAR SECURITIES   </option>
                                                                                    <option value = "BETHEL">   BETHEL EQUITIES P/L  </option>
                                                                                    <option value = "CORP">   CORPSERVE REGISTRARS  </option>
                                                                                    <option value = "DELL">   DELL STOCKBROKERS  </option>
                                                                                    <option value = "EFES">   EFE SECURITIES (PVT) LTD  </option>
                                                                                    <option value = "EGS">   Eagle Stockbrokers  </option>
                                                                                    <option value = "EST">   EASTGATE STOCKBROKERS  </option>
                                                                                    <option value = "FBCS">   FBC SECURITIES (PVT) LTD  </option>
                                                                                    <option value = "FCN">   FALCON STOCKBROKERS  </option>
                                                                                    <option value = "FIN">   FINSEC  </option>
                                                                                    <option value = "IMARA">   IMARA EDWARDS SECURITIES (PVT) LTD  </option>
                                                                                    <option value = "INVS">   INVICTUS SECURITIES ZIMBABWE (PVT) LTD  </option>
                                                                                    <option value = "LES">   LYNTON-EDWARDS STOCKBROKERS(PVT) LTD  </option>
                                                                                    <option value = "MASTS">   MAST STOCKBROKERS (PVT) LTD  </option>
                                                                                    <option value = "MMCS">   MMC STOCKBROKERS  </option>
                                                                                    <option value = "OMCUS">   OLD MUTUAL CUSTODIAL SERVICES  </option>
                                                                                    <option value = "OMSEC">   OLD MUTUAL SECURITIES (PVT) LTD  </option>
                                                                                    <option value = "PLATS">   PLATINUM SECURITIES (PVT) LTD  </option>
                                                                                    <option value = "PLT">   PALLET  </option>
                                                                                    <option value = "SECZ">   Securities Exchange Commission Zimbabwe  </option>
                                                                                    <option value = "STL">   Stallone Securities  </option>
                                                                                    <option value = "STS">   Southern Trust Securities  </option>
                                                                                    <option value = "TAAA">   Tripple Alliance Brokers  </option>
                                                                                    <option value = "WESTS">   WEST STOCKBROKERS  </option>
                                                                                    <option value = "WHSEC">   WATERHOUSE SECURITIES  </option>
                                                                                </select>                                                                    
                                                                            <!-- <input type="text" name  = "" id = "my_price1" class="form-control "  placeholder="Stockbrocking Firm"> -->
                                                                        </div>
                                                                    </div> 

                                                                    <div class="col-sm-4  ">

                                                                    </div>

                                                                    <div class="col-sm-4  ">

                                                                    </div>

                                                                </div>     
                                                            </div>     


                                                            <div class="clearfix"></div>

                                                        </div>

                                                         <!-- end step-->
                                                         <div class="submit step" id="complete">
                                                            <i class="icon-check"></i>
                                                            <h3>Your application is now ready for submission. </h3>
                                                            <button type="submit" name="process" class="submit">Submit the registration details</button>
                                                            
                                                         </div>
                                                         <!-- end submit step -->
                                                         <!-- </div> -->
                                                         <!-- end middle-wizard -->
                                                         <div id="bottom-wizard">
                                                            <button type="button" name="backward" class="backward">Back</button>
                                                            <button type="button" name="forward" class="forward">Next</button>
                                                         </div>
                                                         <!-- end bottom-wizard -->


                                                      </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>

                            </div>

                        </div>


                    </div>
                </div>                  
            </section>


        </section>

        <footer id="footer">
            <marquee>
                <div ng-controller="notLoggedCtrl">
                    <ul class="f-menu">
                            <li ng-repeat=" countrz in marketwatch" ng-bind-html-unsafe="countrz.text" >
                               {{countrz.market_company}} - {{countrz.market_vwap}} ( {{countrz.market_change}} ) 
                            </li>
                    </ul>
                </div>
            </marquee>
        </footer>


        <!-- Page Loader -->
        <div class="page-loader">
            <div class="preloader pls-white">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Connecting to C-Trade...</p>
            </div>
        </div>

        <!-- Libs -->

        <script src="js/jquery.tweet.min.js"></script>
        <script src="js/jquery.bxslider.min.js"></script>
        <script src="js/quantity-bt.js"></script>

        <script src="js/retina.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/function_s.js"></script>
        <!-- FANCYBOX -->
        <script  src="js/fancybox/source/jquery.fancybox.pack.js?v=2.1.4" type="text/javascript"></script> 
        <script src="js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5" type="text/javascript"></script> 
        <script src="js/fancy_func.js" type="text/javascript"></script> 

        <!-- Javascript Libraries -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/app.js"></script>
    
    

    </body>
  </html>