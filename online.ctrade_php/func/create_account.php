<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

//extract data from the post
// extract($_POST);

// investor_type
// occupation
// company_name
// custodian
// cds
// security_ex
// soi
// product
$my_name = "" ; 
$acctype = $_POST['acc_type'] ; 
$broker = "" ;
$surname = $_POST['surname'] ; 
$middlename = "" ; 
$first_name = $_POST['fore_name']; 
$initials = "" ; 
$name_title = $_POST['my_title'] ; 
$country = $_POST['country'] ; 
$city = $_POST['city'] ; 
$tel_no = $_POST['telphone'] ; 
$mobile_no = $_POST['cell_num'] ; 
$currency = "USD" ; 
$id_type = "NID" ; 
if ($acctype =='c'){
$id_type = "Certificate of Incorporation Number" ;   
}
$id_no = $_POST['nat_id'] ; 
$nationality = $_POST['country'] ; 
$date_of_birth = $_POST['dob'] ; 
$gender = $_POST['gender'] ; 
$address_1 = $_POST['add_home'] ; 
$address_2 = $_POST['add_bus'] ; 
$address_3 = "" ; 
$address_4 = "" ; 
$email = $_POST['my_email'] ; 
$dvdnd_payee = "" ; 
$dvdnd_ac_class = $_POST['txt'] ; 
$dvdnd_bank = $_POST['txt'] ; 
$dvdnd_branch =  $_POST['branch'] ;
$dvdnd_ac_no = $_POST['acc_num'] ; 
$dvdnd_id_type = "" ; 
$dvdnd_id_no = "" ; 
$settlement_bank = $_POST['bank_name'] ; 
$settlement_branch = $_POST['branch'] ; 
$settlement_ac_no = $_POST['acc_num'] ; 
$mobile_money = "" ; 
$custodian = $_POST['custodian'] ; 
$sorce = $_POST['source'] ;


$data = $acctype ."-".$surname ."-".$first_name ."-".$name_title ."-".$country ."-".$city ."-".$tel_no ."-".$mobile_no 
."-".$id_no ."-".$nationality ."-".$date_of_birth ."-".$gender ."-".$address_1 ."-".$address_2 ."-".$email ."-".
$settlement_bank ."-".$settlement_branch ."-".$settlement_ac_no ."-".$custodian ."-".$sorce."\n" ;
$fp = fopen('log.txt', 'a');
fwrite($fp, $data);
fclose($fp);

include("../db_cds/DbConnectONLINE.php");
// require_once '../db_cds/PassHash.php';

$my_email  = $_POST['my_email'] ;
$my_pass  = @$_POST['my_pass'] ;
$db_cdsc = new DbConnectCDSC();
$conn_cdsc = $db_cdsc->connect();    

$ttl_users = @$_POST["comp_rep_ttl"] ; 
$createUser = true ; 
$start = new DateTime($_POST['dob']);
 $end = new DateTime(); 
 //Current date time 
 $diff = $start->diff($end);
 $age=$diff->format('%Y');
 $message="";
if(strtoupper($acctype)=="I" && $age<18){
	$createUser = false ; 
$message="Date Of Birth is incorrect , current must be greater than 18 years";
}

if ($acctype =='c'){
        for($i = 1 ; $i < $ttl_users + 1  ; $i++) {
           if(isUserExists($conn_cdsc , @$_POST["comp_rep_email_".$i] )){
                $createUser = false ; 
                break;
            }           
        } 

        
}
if(isUserExists($conn_cdsc ,$my_email)=="1"){
  $createUser = false ; 
  $message="Email already exists";
}


if ($createUser) {



        //set POST variables
        $url = 'http://192.168.3.248/EscrowWebService/EscrowSoapWebService.asmx/Createnewaccount';
        $fields = array(
                'acctype'=>$acctype , 
                'broker'=>$broker , 
                'Custodian'=>$custodian , 
                'SourceName'=>$sorce , 
                'surname'=>$surname , 
                'middlename'=>$middlename , 
                'first_name'=>$first_name , 
                'initials'=>$initials , 
                'name_title'=>$name_title , 
                'country'=>$country , 
                'city'=>$city , 
                'tel_no'=>$tel_no , 
                'mobile_no'=>$mobile_no , 
                'currency'=>$currency , 
                'id_type'=>$id_type , 
                'id_no'=>$id_no , 
                'nationality'=>$nationality , 
                'date_of_birth'=>$date_of_birth , 
                'gender'=>$gender , 
                'address_1'=>$address_1 , 
                'address_2'=>$address_2 , 
                'address_3'=>$address_3 , 
                'address_4'=>$address_4 , 
                'email'=>$email , 
                'dvdnd_payee'=>$dvdnd_payee , 
                'dvdnd_ac_class'=>$dvdnd_ac_class , 
                'dvdnd_bank'=>$dvdnd_bank , 
                'dvdnd_branch'=>$dvdnd_branch , 
                'dvdnd_ac_no'=>$dvdnd_ac_no , 
                'dvdnd_id_type'=>$dvdnd_id_type , 
                'dvdnd_id_no'=>$dvdnd_id_no , 
                'settlement_bank'=>$settlement_bank , 
                'settlement_branch'=>$settlement_branch , 
                'settlement_ac_no'=>$settlement_ac_no , 
                'mobile_money'=>$mobile_money 
                );

        //url-ify the data for the POST
        $fields_string = "" ; 
        foreach($fields as $key=>$value) { 
          $fields_string .= $key.'='.$value.'&'; 
        }
        rtrim($fields_string,'&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_POST,count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

        //execute post
        $result = curl_exec($ch);

        $pin_key = (string) $result ;
        $pin_key = str_replace(" ", "", $pin_key) ;
        // echo $pin_key ;
        $pin  = str_replace('<?xmlversion="1.0"encoding="utf-8"?>' , "" , str_replace("</string>", "", $pin_key)) ; 
        $pin = str_replace('&lt;', ' ', $pin) ;
        $pin = str_replace('&gt;', ' ', $pin) ;
        $pin = str_replace('<stringxmlns="EscrowService">', ' ', $pin) ;
        if (strpos($pin, 'investor_pin') == false) {
            $url  = "../../index.php?sms_err=".urlencode($pin) ;     
            // $url  = str_replace("\n", "", $url) ;
            if($pin == "AccountCreationFailedPleaseContactAdministrator")
              header("Location: ../sign_up.php?message=Account Creation Failed Please Contact Administrator") ; 
              //echo "Account Creation Failed Please Contact Administrator" ; 
            else
              header("Location: ../sign_up.php?message=Account Creation Failed Please Contact Administrator") ; 
              //echo $pin ;     
            // header("Location: ".$url) ; 
        }else{
            $pin  = str_replace("investor_pin" , "" , $pin ) ; 
            $pin  = str_replace(' ', '', $pin) ;
            $pin  = str_replace('\n', '', $pin) ;
            $pin  = substr_replace($pin, "", -1) ; 
            
            // echo "ini". $pin ; 
            // echo "hello". $result ;
            //close connection
            curl_close($ch);


            $my_name  = $surname." ".$first_name ;
            $cds_number  = trim($pin) ;


              include("../db_cds/DbConnectCDS.php");
              $db_cds = new DbConnectCDS();
              $conn_cds = $db_cds->connect();          
              $sqldl ="SELECT t.BrokerCode , ut.Company_name , t.CDS_Number , (SELECT Company_name from Client_Companies where Company_Code = t.Custodian ) as Custodian_name from 
        Accounts_Clients_Web t , Client_Companies ut WHERE t.Custodian = ut.Company_Code and  t.IDNoPP = '".$id_no."' order by t.ID desc";
        // echo $sqldl ;
              $result = sqlsrv_query($conn_cds,$sqldl);
              $broker_send = "" ; 
              $cds_number = "" ; 
              $custodian_send = "" ; 
                if( $result === false ){
                     echo 'failed to load details ' ;  
                     die( print_r( sqlsrv_errors(), true));
                }else{
                     while ($row = sqlsrv_fetch_array($result)){     
                          $broker_send =  $row['Company_name'] ;  
                          $cds_number =  $row['CDS_Number']; 
                          $custodian_send =  $row['Custodian_name']; 
                          break ;  
                     }
                }
              if($cds_number == "") {
                $cds_number = "0000/1" ; 
              }
			  // required headers

			  //upload files
			  try {
if(!empty($_FILES["Profile"]["name"])){
	
$profilePhoto = $_FILES["Profile"]["tmp_name"];
$profilePhototype = pathinfo(basename($_FILES["Profile"]["name"]), PATHINFO_EXTENSION);
//need to get the content of the file
$fp = fopen($profilePhoto, 'r');
$file_content = fread($fp, filesize($profilePhoto));
$profilePhoto_content = $file_content;
fclose($fp);
$profilephotoquery  =   "insert into Accounts_Documents([doc_generated],[Name],[ContentType],[Data],[Email]) values(?,'Photo',?,CAST(? AS varbinary(max)),?)" ;
$result=sqlsrv_query($conn_cds,$profilephotoquery,array($cds_number,$profilePhototype,$profilePhoto_content,$email));
	
}



}
//catch exception
catch(Exception $e) {

}
try {
if(!empty($_FILES["NationalIdd"]["name"])){

$nationalIDD = $_FILES["NationalIdd"]["tmp_name"];
$nationalIDDtype = pathinfo(basename($_FILES["NationalIdd"]["name"]), PATHINFO_EXTENSION);
//nationalidquery
$fp2 = fopen($nationalIDD, 'r');
$file_content = fread($fp2, filesize($nationalIDD));
$nationalIDD_content = $file_content;
fclose($fp2);
 $nationalidquery  =   "insert into Accounts_Documents([doc_generated],[Name],[ContentType],[Data],[Email]) values(?,'National ID',?,CAST(? AS varbinary(max)),?)" ;
 $result2=sqlsrv_query($conn_cds,$nationalidquery,array($cds_number,$nationalIDDtype,$nationalIDD_content,$email));
	
}

  
}
//catch exception
catch(Exception $e) {

}
	
try {
if(!empty($_FILES["ProofOfResidence"]["name"])){
	
$proofOfResidence = $_FILES["ProofOfResidence"]["tmp_name"];
$proofOfResidencetype = pathinfo(basename($_FILES["ProofOfResidence"]["name"]), PATHINFO_EXTENSION);
//proofresidence
$fp3 = fopen($proofOfResidence, 'r');
$file_content = fread($fp3, filesize($proofOfResidence));
$proofOfResidence_content = $file_content;
fclose($fp3);
 $proofresidencequery  =   "insert into Accounts_Documents([doc_generated],[Name],[ContentType],[Data],[Email]) values(?,'Proof Of Residence',?,CAST(? AS varbinary(max)),?)" ;
 $result3=sqlsrv_query($conn_cds,$proofresidencequery,array($cds_number,$proofOfResidencetype,$proofOfResidence_content,$email));

}



}

//catch exception
catch(Exception $e) {

}

//or 2
			  try {
if(!empty($_FILES["Profile2"]["name"])){
	
$profilePhoto = $_FILES["Profile2"]["tmp_name"];
$profilePhototype = pathinfo(basename($_FILES["Profile2"]["name"]), PATHINFO_EXTENSION);
//need to get the content of the file
$fp4 = fopen($profilePhoto, 'r');
$file_content = fread($fp4, filesize($profilePhoto));
$profilePhoto_content = $file_content;
fclose($fp4);
$profilephotoquery  =   "insert into Accounts_Documents([doc_generated],[Name],[ContentType],[Data],[Email]) values(?,'Photo',?,CAST(? AS varbinary(max)),?)" ;
$result=sqlsrv_query($conn_cds,$profilephotoquery,array($cds_number,$profilePhototype,$profilePhoto_content,$email));
	
}



}
//catch exception
catch(Exception $e) {

}
try {
if(!empty($_FILES["NationalIdd2"]["name"])){
	
$nationalIDD = $_FILES["NationalIdd2"]["tmp_name"];
$nationalIDDtype = pathinfo(basename($_FILES["NationalIdd2"]["name"]), PATHINFO_EXTENSION);
//nationalidquery
$fp5 = fopen($nationalIDD, 'r');
$file_content = fread($fp5, filesize($nationalIDD));
$nationalIDD_content = $file_content;
fclose($fp5);
 $nationalidquery  =   "insert into Accounts_Documents([doc_generated],[Name],[ContentType],[Data],[Email]) values(?,'National ID',?,CAST(? AS varbinary(max)),?)" ;
 $result2=sqlsrv_query($conn_cds,$nationalidquery,array($cds_number,$nationalIDDtype,$nationalIDD_content,$email));
	
}

  
}
//catch exception
catch(Exception $e) {

}
	
try {
if(!empty($_FILES["ProofOfResidence2"]["name"])){
	
$proofOfResidence = $_FILES["ProofOfResidence2"]["tmp_name"];
$proofOfResidencetype = pathinfo(basename($_FILES["ProofOfResidence2"]["name"]), PATHINFO_EXTENSION);
//proofresidence
$fp6 = fopen($proofOfResidence, 'r');
$file_content = fread($fp6, filesize($proofOfResidence));
$proofOfResidence_content = $file_content;
fclose($fp6);
 $proofresidencequery  =   "insert into Accounts_Documents([doc_generated],[Name],[ContentType],[Data],[Email]) values(?,'Proof Of Residence',?,CAST(? AS varbinary(max)),?)" ;
 $result3=sqlsrv_query($conn_cds,$proofresidencequery,array($cds_number,$proofOfResidencetype,$proofOfResidence_content,$email));

}



}

//catch exception
catch(Exception $e) {

}

            if ($acctype =='c'){

			$my_email=$_POST['my_email'];

$my_name=$_POST['surname'];
$mobile_no=$_POST['cell_num'] ;
$my_pass  = $_POST['my_pass'];
             $responses ="https://demo.ctrade.co.zw/mobileapi/SubscribersInsert?username=".$my_email."&email=".$my_email."&password=".$my_pass."&phone=".$mobile_no."&cdsnumber=".$cds_number."&fullname=".$my_name."&comp_authorizer=yes&comp_initiator=yes";
        //$response = "https://google.co.zw/"
$ch = curl_init();
 
        // set url
        curl_setopt($ch, CURLOPT_URL,$responses);
 
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false) ;
 		//curl_setopt($ch,CURLOPT_POST,count(8));
        //curl_setopt($ch,CURLOPT_POSTFIELDS,);
        // $output contains the output string
        $output = curl_exec($ch);

  echo $output;
        // close curl resource to free up system resources
        curl_close($ch); 
     
        
                $ttl_users = @$_POST["comp_rep_ttl"] ; 

                //echo $ttl_users ."<br>"; 

  
                for($i = 1 ; $i < $ttl_users + 1  ; $i++) {
                    $rep_name  = @$_POST["comp_rep_name_".$i] ; 
                    $rep_email  = @$_POST["comp_rep_email_".$i] ; 
                    $rep_phone  = @$_POST["comp_rep_phone_".$i] ; 
                    $rep_pass  = @$_POST["comp_rep_pass_".$i] ; 
                    $rep_initiator  = @$_POST["comp_rep_initiator_".$i] ; 
                    $rep_authorizer  = @$_POST["comp_rep_authorizer_".$i] ; 
              
						  
	$responses ="https://demo.ctrade.co.zw/mobileapi/SubscribersInsert?username=".$rep_email."&email=".$rep_email."&password=".$rep_pass."&phone=".$rep_phone."&cdsnumber=".$cds_number."&fullname=".$rep_name."&comp_authorizer=".$rep_initiator."&comp_initiator=".$rep_authorizer;
 //$response = "https://google.co.zw/"
$ch = curl_init();
 
        // set url
        curl_setopt($ch, CURLOPT_URL,$responses);
 
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false) ;
 		//curl_setopt($ch,CURLOPT_POST,count(8));
        //curl_setopt($ch,CURLOPT_POSTFIELDS,);
        // $output contains the output string
        $output = curl_exec($ch);

  echo $output;
        // close curl resource to free up system resources
        curl_close($ch); 
     
       
      
                } 
              }else{

$my_email=$_POST['my_email'];

$my_name=$_POST['surname'];
$mobile_no=$_POST['cell_num'] ;
$my_pass  = $_POST['my_pass'];
$responses ="https://demo.ctrade.co.zw/mobileapi/SubscribersInsert?username=".$my_email."&email=".$my_email."&password=".$my_pass."&phone=".$mobile_no."&cdsnumber=".$cds_number."&fullname=".$my_name."&comp_authorizer=yes&comp_initiator=yes";
//$response = "https://google.co.zw/"
$ch = curl_init();
 
        // set url
        curl_setopt($ch, CURLOPT_URL,$responses);
 
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false) ;
 		//curl_setopt($ch,CURLOPT_POST,count(8));
        //curl_setopt($ch,CURLOPT_POSTFIELDS,);
        // $output contains the output string
        $output = curl_exec($ch);

  //echo $output;
        // close curl resource to free up system resources
        curl_close($ch); 
     
     
    
                }

              //$result=sqlsrv_query($conn_cdsc,$sql);
              // echo  $sql ; 
             header("Location: ../sign_up.php?message=Account successfully created") ; 
               //header("Location: ../sign_up.php?message=".$output) ; 
             
			  //echo "" ;

        }

} else {
 header("Location: ../sign_up.php?message=".$message) ; 
}




function isUserExists($consn , $email) {
    $sqll ="SELECT id from Vatenkecis WHERE Email = '".$email."'";
    $responses ="https://demo.ctrade.co.zw/mobileapi/CheckIfUserExists?Email=".$email;

$ch = curl_init();
 
        // set url
        curl_setopt($ch, CURLOPT_URL,$responses);
 
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false) ;
 		//curl_setopt($ch,CURLOPT_POST,count(8));
        //curl_setopt($ch,CURLOPT_POSTFIELDS,);
        // $output contains the output string
        $output = curl_exec($ch);

  
        // close curl resource to free up system resources
        curl_close($ch); 
     
    return $output;
}



?>