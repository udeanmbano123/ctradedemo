
<?php

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}


$email = $_POST['email'] ; 
$sms = $_POST['message'] ; 
$o_num = $_POST['order_number'] ;

$plain_txt = $email."|".$o_num ; 
$encrypted_txt = encrypt_decrypt('encrypt', $plain_txt);

sendEmail_($email  , $encrypted_txt , $sms) ; 



function sendEmail_($email  , $password , $sms){
  require_once("../sendmail.php") ;

  $mailerThis = new sendMail() ;

$message =<<<EEF
          <!doctype html>
          <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
             <head>
                <title></title>
                <!--[if !mso]><!-- -->
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <!--<![endif]-->
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <style type="text/css">
                   #outlook a { padding: 0; }
                   .ReadMsgBody { width: 100%; }
                   .ExternalClass { width: 100%; }
                   .ExternalClass * { line-height:100%; }
                   body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                   table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                   img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
                   p { display: block; margin: 13px 0; }
                </style>
                <!--[if !mso]><!-->
                <style type="text/css">
                   @media only screen and (max-width:480px) {
                   @-ms-viewport { width:320px; }
                   @viewport { width:320px; }
                   }
                </style>
                <!--<![endif]-->
                <!--[if mso]>
                <xml>
                   <o:OfficeDocumentSettings>
                      <o:AllowPNG/>
                      <o:PixelsPerInch>96</o:PixelsPerInch>
                   </o:OfficeDocumentSettings>
                </xml>
                <![endif]-->
                <!--[if lte mso 11]>
                <style type="text/css">
                   .outlook-group-fix {
                   width:100% !important;
                   }
                </style>
                <![endif]-->
                <!--[if !mso]><!-->
                <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
                <style type="text/css">
                   @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
                </style>
                <!--<![endif]-->
                <style type="text/css">
                   @media only screen and (min-width:480px) {
                   .mj-column-per-100 { width:100%!important; }
                   }
                </style>
                <style type="text/css">
                   @media only screen and (max-width:480px) {
                   .mj-hero-content {
                   width: 100% !important;
                   }
                   }
                </style>
             </head>
             <body style="background: #f5f6fa;">
                <div style="background-color:#f5f6fa;">
                   <!--[if mso | IE]>
                   <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                      <tr>
                         <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                            <![endif]-->
                            <div style="margin:0px auto;max-width:600px;">
                               <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0">
                                  <tbody>
                                     <tr>
                                        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:20px;padding-top:30px;">
                                           <!--[if mso | IE]>
                                           <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                              <tr>
                                                 <td style="vertical-align:undefined;width:600px;">
                                                    <![endif]-->
                                                    <div class="" style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:40px;text-align:center;">
                                                       <a href="#" class="ks-logo" style="font-size: 40px; text-decoration: none; color: #5ea6ca; font-weight: bold;padding:40px;"><img src="http://finsec.co.zw/onlinetrade/img/finlogoz.png" width="50%"></a>
                                                    </div>
                                                    <!--[if mso | IE]>
                                                 </td>
                                              </tr>
                                           </table>
                                           <![endif]-->
                                        </td>
                                     </tr>
                                  </tbody>
                               </table>
                            </div>
                            <!--[if mso | IE]>
                         </td>
                      </tr>
                   </table>
                   <![endif]-->
                   <!--[if mso | IE]>
                   <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                      <tr>
                         <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                            <![endif]--><!--[if mso | IE]>
                            <v:image xmlns:v="urn:schemas-microsoft-com:vml" croptop="0" cropbottom="0" style="width:435pt;height:180pt;position:absolute;top:0;mso-position-horizontal:center;border:0;z-index:-3;" src="../admin/default-primary/assets/img/placeholders/placeholder-640x480.png" />
                            <![endif]--><!--[if mso | IE]>
                         </td>
                      </tr>
                   </table>
                   <![endif]-->
                   <!--[if mso | IE]>
                   <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                      <tr>
                         <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                            <![endif]-->
                            <div style="margin:0px auto;max-width:600px;background:#fff;">
                               <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#fff;" align="center" border="0">
                                  <tbody>
                                     <tr>
                                        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;">
                                           <!--[if mso | IE]>
                                           <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                              <tr>
                                                 <td style="vertical-align:top;width:600px;">
                                                    <![endif]-->
                                                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                                                       <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                                          <tbody>
                                                             <tr>
                                                                <td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="left">
                                                                   <div class="" style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;">
                                                                      <p style="font-size: 14px; color: #333; margin: 5px 0;">{$sms}</p>
                                                                      <p style="font-size: 14px; color: #333; margin: 5px 0;">
                                                                         <a href="https://ctrade.co.zw/ctrade/online.ctrade_php/func/withdrawal_corp.php?id={$password}&opt=accept">Click here to authorise</a>
                                                                      </p>                        
                                                                      <p style="font-size: 14px; color: #333; margin: 5px 0;">
                                                                         <a href="https://ctrade.co.zw/ctrade/online.ctrade_php/func/withdrawal_corp.php?id={$password}&opt=reject">Click here to reject</a>
                                                                      </p>                        
                                                                   </div>
                                                                </td>
                                                             </tr>

                                                          </tbody>
                                                       </table>
                                                    </div>
                                                    <!--[if mso | IE]>
                                                 </td>
                                              </tr>
                                           </table>
                                           <![endif]-->
                                        </td>
                                     </tr>
                                  </tbody>
                               </table>
                            </div>
                            <!--[if mso | IE]>
                         </td>
                      </tr>
                   </table>
                   <![endif]-->

                   <!--[if mso | IE]>
                   <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                      <tr>
                         <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                            <![endif]-->
                            <div style="margin:0px auto;max-width:600px;"></div>
                            <!--[if mso | IE]>
                         </td>
                      </tr>
                   </table>
                   <![endif]-->
                </div>
             </body>
          </html>
EEF;



  $sendMail = $mailerThis->sendEmail($email, $message, 'C-TRADE Coorporate Withdrawal Transaction Authorisation' , '' );

  // $headers = "From: no-reply@finsec.co.zw\r\n";
  // $headers .= "Reply-To: no-reply@finsec.co.zw\r\n";
  // $headers .= "BCC: tinashe@finsec.co.zw\r\n";
  // $headers .= "MIME-Version: 1.0\r\n";
  // $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

  // mail($email, $subject, $message, $headers);

  }


?>