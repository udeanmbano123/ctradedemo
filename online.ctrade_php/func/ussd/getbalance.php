  <?php
	include("../../db_cds/DbConnectCDS.php");
	$db_cds = new DbConnectCDS();
	$conn_cds = $db_cds->connect(); 

	$phone_num = (strlen($_POST['phone'])>9)?substr($_POST['phone'], -9):$_POST['phone'];
  	$cds_num = getCDSNumber($conn_cds, $phone_num) ; 
  	//echo $cds_num  ;
  	$arry =  getBalance($conn_cds, $cds_num) ; 

  	if(count($arry) > 0){ 
	echo "C-TRADE Balance\nUncleared Cash: ".  
		asDollars($arry[0]['VirtCashBal']) . "\nCleared Cash: ".   
		asDollars($arry[0]['ActualCashBal']) . "\nCash Available: ".  
		asDollars($arry[0]['CashBal']) . "\nMy Portfolio: ".   
		asDollars($arry[0]['MyPotValue']). "\nTotal Account: ". 
		asDollars($arry[0]['TotalAll']). "\nPortfolio Gain: ".  
		asDollars($arry[0]['MyProfitLoss'])  ; 
	}else{
		echo 'Unable to load your balance' ;
	}

  function getBalance($conn_cds, $cdsNumber ){
    
	$sqldl = "SELECT
                  isnull(SUM(totAllShares * currePrice), 0) as MyPotValue,
                  ISNULL(SUM((totAllShares * currePrice) - (prevdayQuantity *PrevPrice) ),0) AS MyProfitLoss,
                  (select isnull(sum(Amount), 0) from [CDSC].[dbo].CashTrans where CDS_Number = '" . $cdsNumber ."' ) as CashBal , 
                   (select isnull(sum(Amount), 0) from [CDSC].[dbo].CashTrans where CDS_Number = '". $cdsNumber ."' and [TransStatus] = '2' ) as VirtCashBal ,
                   (select isnull(sum(Amount), 0) from [CDSC].[dbo].CashTrans where CDS_Number = '". $cdsNumber ."' and [TransStatus] = '1' ) as ActualCashBal
                  FROM[CDSC].[dbo].[PortfolioAll] WHERE CDS_NUMBER = '".$cdsNumber ."'
                "; 
    //echo $sqldl . "<br>" ; 
    $arr = array();
	$result = sqlsrv_query($conn_cds,$sqldl);
	if($result === false ){
		return $arr ;  
	}else{
		while ($row = sqlsrv_fetch_array($result)){
				$arr[0]["VirtCashBal"] = $row['VirtCashBal'] ; 
				$arr[0]["ActualCashBal"] = $row['ActualCashBal'] ; 
				$arr[0]["CashBal"] = $row['VirtCashBal'] ; 
				$arr[0]["MyPotValue"] = $row['MyPotValue'] ; 
				$arr[0]["TotalAll"] = $row['CashBal'] + $row['MyPotValue']  ; 
				$arr[0]["MyProfitLoss"] = $row['MyProfitLoss'] ; 
				return $arr  ;
		    break ;  
		}
		return $arr ;    
	}
  }
	function asDollars($value) {
	  return '$' . number_format($value, 2);
	}

  function getCDSNumber($conn_cds, $phone){

	$sqldl ="SELECT * FROM [CDS_ROUTER].[dbo].[Accounts_Clients_Web] WHERE Mobile like '%".$phone."%'";
	$result = sqlsrv_query($conn_cds,$sqldl);
	if( $result === false ){
			return "NOCDSNUM" ;
	}else{
		while ($row = sqlsrv_fetch_array($result)){
			return  $row['CDS_Number']  ;
		    break ;  
		}
	}

	}
  ?>