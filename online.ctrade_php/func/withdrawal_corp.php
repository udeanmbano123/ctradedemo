
<font color='green'>This transaction has been </font>

<?php 



/*
 * simple method to encrypt or decrypt a plain text string
 * initialization vector(IV) has to be the same when encrypting and decrypting
 * 
 * @param string $action: can be 'encrypt' or 'decrypt'
 * @param string $string: string to encrypt or decrypt
 * id, cds_number, email, accept, order_id, email_send, company, OrderPlacer, order_via, order_type
 * @return string
 */

$encry_text = $_GET['id'] ;
$opt_text = $_GET['opt'] ;
function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}



$decrypted_txt = encrypt_decrypt('decrypt', $encry_text);

$lists_them = explode("|", $decrypted_txt) ; 

//echo "Decrypted Text =" .$decrypted_txt. "<br>";

$email = @$lists_them[0] ; 
$id_num = @$lists_them[1] ;


include("../db_cds/DbConnectONLINE.php");

$db_cdsc = new DbConnectCDSC();
$conn_cdsc = $db_cdsc->connect();   

if($opt_text == "accept"){
	 //echo searchIFDONE($conn_cdsc , $id_num);
   
	 setAuthorisation($conn_cdsc , $email , $id_num) ; 
	 if(!searchIFDONE($conn_cdsc , $id_num)){
		 
		if(searchIFORDEROPEN($conn_cdsc , $id_num)){
		
        	setDoneTrue($conn_cdsc , $id_num) ; 
    	}
    }
    echo "authorised" ; 
}else{    
    setOrderRejected($conn_cdsc , $id_num) ; 
    echo "rejected" ;
}


function setAuthorisation($conn , $email , $id_num){
	$sql  = "UPDATE [CDSC].[dbo].[accounts_auth] SET Accept = '1' WHERE email=? and order_id=? " ; 
	$result1 = sqlsrv_query($conn,$sql,Array($email,$id_num));
	//echo "<br>".$sql  ; 
}

function setDoneTrue($conn , $id_num){

    $sql  = "UPDATE [cdsc].[dbo].[cashtranstemps] SET [TransStatus] = '1' WHERE [ID]='$id_num' INSERT INTO cdsc.dbo.cashtrans([Description], [TransType], [Amount], [DateCreated], [TransStatus], [CDS_Number], [Paid], [Reference]) select [Description], [TransType], [Amount], [DateCreated], [TransStatus], [CDS_Number], [Paid], [Reference] from cdsc.dbo.cashtranstemps where id ='$id_num'" ; 

    $result11 = sqlsrv_query($conn,$sql);

}
function setOrderRejected($conn , $id_num){
	$sql2  = "UPDATE [CDSC].[dbo].[cashtranstemp] SET Accept=0 and Reject = '1' WHERE id=? " ; 
	//echo "<br>".$sql2 ; 
	$result1 = sqlsrv_query($conn,$sql2,Array($id_num));
}

function searchIFDONE($conn , $id_num) {
	$sql1 = "SELECT * FROM [CDSC].[dbo].[accounts_auth] WHERE  order_id=? and accept = '0' " ; 
	$result = sqlsrv_query($conn,$sql1,Array($id_num));
    $rows = sqlsrv_has_rows( $result );
	//echo "<br>row count =".$rows."<br>".$sql1 ; 
	return $rows ; 
}
function searchIFORDEROPEN($conn , $id_num) {
    //$sql1 = "SELECT * FROM [testcds_ROUTER].[dbo].[Pre_Order_Live] WHERE  [BrokerRef]='$id_num' and [OrderStatus] = 'NOT AUTHORISED' and Client_Type = 'c' " ; 
    $sql1 = "SELECT * FROM [CDSC].[dbo].[cashtranstemps] WHERE id = ? and [TransStatus] = '0'" ; 
	$result = sqlsrv_query($conn,$sql1,Array($id_num));
    $rows = sqlsrv_has_rows( $result );
	//echo "<br>row count =".$rows."<br>".$sql1 ; 
	return $rows ; 
}

?>