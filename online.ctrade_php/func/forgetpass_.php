<?php
$my_email  = $_GET['email'] ;
$encry_text = $my_email ; 

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}
$decrypted_txt = encrypt_decrypt('encrypt', $encry_text);
$ch = curl_init(); 
curl_setopt($ch, CURLOPT_URL, "https://ctrade.co.zw/api/sendmail?emailAdd=".$my_email."&emailbody=Copy%20the%20link%20and%20paste%20to%20your%20browser%20to%20reset%20password%20https://ctrade.co.zw/online/reset.php?email=".$decrypted_txt."&subject=Reset%20Password%20Link"); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
$output = curl_exec($ch); 
curl_close($ch);  

echo $output ; 

?>